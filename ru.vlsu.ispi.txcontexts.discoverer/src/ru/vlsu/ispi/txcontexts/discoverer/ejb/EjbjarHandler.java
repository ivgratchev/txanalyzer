package ru.vlsu.ispi.txcontexts.discoverer.ejb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

public class EjbjarHandler extends DefaultHandler {

    protected boolean isEjbName;
    protected boolean isMethodName;
    protected boolean isTransAttribute;
    protected boolean isAssemblyDescriptor;
    protected boolean isEjbClass;
    protected boolean isTransactionType;
    protected boolean isSessionType;
    protected boolean isMethodParams;
    protected boolean isMethodParam;
    protected boolean isMethod;
    protected boolean isEnterpriseBeans;
    protected boolean isSession;

    private Map<String, String> ejbNamesToClasses = new HashMap<String, String>();
    private Map<String, String> classNamesToTMs = new HashMap<String, String>();
    private Map<String, String> classNamesToKinds = new HashMap<String, String>();

    private Map<String, String> methodsToTAs = new HashMap<String, String>();

    protected HashMap<String, HashMap<String, String>> occurenciesMap = new HashMap<String, HashMap<String, String>>();
    protected boolean isContainerTransaction;

    private EjbJarExplorer discoverer;
    
    protected String ejbName = null;
    protected String className = null;
    protected String TA = null;
    protected String TM = null;
    protected String kind = null;
    protected String methodName = null;
    protected String methodParams = null;
 
    @Override
    public void startDocument() throws SAXException {
        System.out.println("Начало обнаружения транзакций");
    }

    @Override
    public void endDocument() throws SAXException {
        System.out.println("Конец обнаружения транзакций:\n");

        discoverer.setClassNamesToKinds(classNamesToKinds);
        discoverer.setClassNamesToTMs(classNamesToTMs);
        discoverer.setEjbNamesToClasses(ejbNamesToClasses);
        discoverer.setMethodsToTAs(methodsToTAs);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        //super.endElement(uri, localName, qName);
        if (qName.equals("assembly-descriptor")) {
            isAssemblyDescriptor = false;
        }
        if (qName.equals("container-transaction")) {
            String fullyQualifiedMethodSignature = ejbNamesToClasses.get(ejbName);
            fullyQualifiedMethodSignature += " :: " + methodName;
            
            methodParams = methodParams!=null?methodParams:"";
            if(!methodName.equals("*")){
                fullyQualifiedMethodSignature += "(" + methodParams + ")";
            }

            methodsToTAs.put(fullyQualifiedMethodSignature, TA);

            methodName = null;
            methodParams = null;
            isContainerTransaction = false;
        }
        if (qName.equals("ejb-name")) {
            // System.out.println("        Имя компонента");
            isEjbName = false;
        }
        if (qName.equals("method")) {
           // System.out.println();
            isMethod = false;
        }
        if (qName.equals("method-name")) {
            isMethodName = false;
        }
        if (qName.equals("trans-attribute")) {
            //   System.out.println("        Транзакционный атрибут");
            isTransAttribute = false;
        }
        if (qName.equals("ejb-class")) {
            // System.out.println("        Класс полнокв компонента");
            isEjbClass = false;
        }
        if (qName.equals("transaction-type")) {
            //   System.out.println("        Тип управления транзакциями");
            isTransactionType = false;
        }
        if (qName.equals("session-type")) {
            //  System.out.println("        Тип компонента");
            isSessionType = false;
        }
        if (qName.equals("method-params")) {
            //   System.out.println("Блок параметров метода");
           // System.out.println();
            isMethodParams = false;
        }
        if (qName.equals("method-param")) {
            //    System.out.println("Параметры метода");
            isMethodParam = false;
        }
        if (qName.equals("enterprise-beans")) {
         //   System.out.println("    Описание компонентов");
            isEnterpriseBeans = false;
          //  System.out.println();
        }

        if (qName.equals("session")) {
            /*
             * private Map<String, String> ejbNamesToClasses = new HashMap<String, String>();
                private Map<String, String> classNamesToTMs = new HashMap<String, String>();
                private Map<String, String> classNamesToKinds = new HashMap<String, String>();

                   private Map<String, Map<String, String>> classNamesToMethodsWithTAs = new HashMap<String, Map<String, String>>();
             */

            ejbNamesToClasses.put(ejbName, className);
            classNamesToTMs.put(className, TM);
            classNamesToKinds.put(className, kind);

            ejbName = null;
            className = null;
           // TA = null;
            TM = null;
            kind = null;

            isSession = false;
           // System.out.println();
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("assembly-descriptor")) {
            isAssemblyDescriptor = true;
        }
        if (qName.equals("container-transaction")) {
          //  System.out.println("    Обнаружена транзакция:");
            isContainerTransaction = true;
        }
        if (qName.equals("ejb-name")) {
            // System.out.println("        Имя компонента");
            isEjbName = true;
        }
        if (qName.equals("method")) {
           // System.out.println("Блок описания метода");
            isMethod = true;
        }
        if (qName.equals("method-name")) {
            isMethodName = true;
        }
        if (qName.equals("trans-attribute")) {
            //   System.out.println("        Транзакционный атрибут");
            isTransAttribute = true;
        }
        if (qName.equals("ejb-class")) {
            // System.out.println("        Класс полнокв компонента");
            isEjbClass = true;
        }
        if (qName.equals("transaction-type")) {
            //   System.out.println("        Тип управления транзакциями");
            isTransactionType = true;
        }
        if (qName.equals("session-type")) {
            //  System.out.println("        Тип компонента");
            isSessionType = true;
        }
        if (qName.equals("method-params")) {
            //   System.out.println("Блок параметров метода");
            isMethodParams = true;
        }
        if (qName.equals("method-param")) {
            //    System.out.println("Параметры метода");
            isMethodParam = true;
        }
        if (qName.equals("enterprise-beans")) {
            System.out.println("    Описание компонентов");
            isEnterpriseBeans = true;
        }

        if (qName.equals("session")) {
            isSession = true;
        }

    }

    @Override
    public void characters(char[] ch, int start, int length) throws
            SAXException {
        String s = new String(ch, start, length);
        s = s.trim();
        if (s.length() > 0) {

            if (isEnterpriseBeans) {
                if (isSession) {
                    if (isEjbName) {
                      //  System.out.println("    Имя: " + s);
                        ejbName = s;
                    }
                    if (isEjbClass) {
                        className = s;
                     //   System.out.println("    Класс: " + s);
                    }
                    if (isSessionType) {
                        kind = s;
                     //   System.out.println("    Тип: " + s);
                    }
                    if (isTransactionType) {
                        TM = s;
                     //   System.out.println("    ТипУправления: " + s);
                    }
                }
            }
            if (isAssemblyDescriptor) {
                if (isContainerTransaction) {
                    if (isMethod) {
                        if (isEjbName) {
                            ejbName = s;
                            System.out.print(s + " # ");
                        }
                        if (isMethodName) {
                            methodName = s;
                        }

                        if (isMethodParams) {
                            if (isMethodParam) {
                                if(methodParams==null){
                                  methodParams = s;
                                }
                                else{
                                    methodParams += ","+s;
                                }
                            System.out.print(s + ",");
                            }
                        }
                    }
                    if (isTransAttribute) {
                        TA = s;
                        System.out.print(" ТА: " + s);
                    }
                }
            }
        }
    }

    /**
     * @return the discoverer
     */
    public EjbJarExplorer getDiscoverer() {
        return discoverer;
    }

    /**
     * @param discoverer the discoverer to set
     */
    public void setDiscoverer(EjbJarExplorer discoverer) {
        this.discoverer = discoverer;
    }
}

/*
 *  if (s.length() > 0) {
            if (isAssemblyDescriptor) {
                if (isEjbName) {
                    System.out.println("        Компонент: " + s);
                    isEjbName = false;
                }
                if (isEjbClass) {
                    System.out.println("        Класс: " + s);
                    isEjbClass = false;
                }
                if (isMethod) {
                    if(isEjbName){
                        System.out.println("        Обнаружен EJB c методом: " + s);
                        isEjbName = false;
                    }
                    isMethod = false;
                }
                if (isMethodName) {
                    System.out.println("        Метод: " + new String(s.equals("*") ? "все методы" : s));
                    isMethodName = false;
                }
                if (isTransAttribute) {
                    System.out.println("        Транзакционный атрибут: " + s);
                    isTransAttribute = false;
                }
            }
 */
