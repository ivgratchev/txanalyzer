package ru.vlsu.ispi.txcontexts.discoverer.ejb;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Andrey
 */
public class EjbJarExplorer {

    private Map<String, String> ejbNamesToClasses;
    private Map<String, String> classNamesToTMs;
    private Map<String, String> classNamesToKinds;
    private Map<String, String> methodsToTAs;

    private void parseEjbJar(String fileName, EjbjarSAXParser parser) {
        File file = new File(fileName);
        try {
            parser.parse(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EjbJarExplorer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EjbJarExplorer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void exploreEjbJar(String fileName) {
        EjbjarSAXParser parser = new EjbjarSAXParser();
        EjbjarHandler handler = (EjbjarHandler) parser.getDefaultHandler();

        handler.setDiscoverer(this);

        parseEjbJar(fileName, parser);

        for (String name : methodsToTAs.keySet()) {
            System.out.println(name + " " + methodsToTAs.get(name));
        }

        System.out.println("++++++++++++++++++");
        for (String name : classNamesToKinds.keySet()) {
            System.out.println(name + " " + classNamesToKinds.get(name));
        }

        System.out.println("++++++++++++++++++");
        for (String name : classNamesToTMs.keySet()) {
            System.out.println(name + " " + classNamesToTMs.get(name));
        }
       /* ejbNamesToClasses = ;
        classNamesToTMs;
        classNamesToKinds;
        methodsToTAs; */
        
    }

    public static void main(String[] args) throws Exception {
        //(new Main()).discoverEjbJar("/home/andrey/windows_drives/D/__eclipse_clean/workspace/CarnetContactsEJB31/WebContent/WEB-INF/ejb-jar.xml");
     EjbJarExplorer m = new EjbJarExplorer();
     m.exploreEjbJar("/home/andrey/windows_drives/D/__eclipse_clean/workspace/CarnetContactsEJB31/WebContent/WEB-INF/ejb-jar.xml");
    }

    /**
     * @return the ejbNamesToClasses
     */
    public Map<String, String> getEjbNamesToClasses() {
        return ejbNamesToClasses;
    }

    /**
     * @param ejbNamesToClasses the ejbNamesToClasses to set
     */
    public void setEjbNamesToClasses(Map<String, String> ejbNamesToClasses) {
        this.ejbNamesToClasses = ejbNamesToClasses;
    }

    /**
     * @return the classNamesToTMs
     */
    public Map<String, String> getClassNamesToTMs() {
        return classNamesToTMs;
    }

    /**
     * @param classNamesToTMs the classNamesToTMs to set
     */
    public void setClassNamesToTMs(Map<String, String> classNamesToTMs) {
        this.classNamesToTMs = classNamesToTMs;
    }

    /**
     * @return the classNamesToKinds
     */
    public Map<String, String> getClassNamesToKinds() {
        return classNamesToKinds;
    }

    /**
     * @param classNamesToKinds the classNamesToKinds to set
     */
    public void setClassNamesToKinds(Map<String, String> classNamesToKinds) {
        this.classNamesToKinds = classNamesToKinds;
    }

    /**
     * @return the methodsToTAs
     */
    public Map<String, String> getMethodsToTAs() {
        return methodsToTAs;
    }

    /**
     * @param methodsToTAs the methodsToTAs to set
     */
    public void setMethodsToTAs(Map<String, String> methodsToTAs) {
        this.methodsToTAs = methodsToTAs;
    }
}
