/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.vlsu.ispi.txcontexts.discoverer.ejb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Andrey
 */

public class EjbjarSAXParser {

    public EjbjarSAXParser(){
    try {
            saxParserFactory = SAXParserFactory.newInstance();
            contactSaxParser = saxParserFactory.newSAXParser();
            defaultHandler = new EjbjarHandler();
            ((EjbjarHandler) defaultHandler).setDiscoverer(discoverer);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    SAXParserFactory saxParserFactory;
    SAXParser contactSaxParser;
    private DefaultHandler defaultHandler;

    private EjbJarExplorer discoverer;

    public void parse(File file) throws FileNotFoundException, IOException {
        try {
            contactSaxParser.parse(file, getDefaultHandler());
        } catch (SAXException ex) {
            Logger.getLogger(EjbjarSAXParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the defaultHandler
     */
    public DefaultHandler getDefaultHandler() {
        return defaultHandler;
    }

    /**
     * @return the discoverer
     */
    public EjbJarExplorer getDiscoverer() {
        return discoverer;
    }

    /**
     * @param discoverer the discoverer to set
     */
    public void setDiscoverer(EjbJarExplorer discoverer) {
        this.discoverer = discoverer;
    }
}
