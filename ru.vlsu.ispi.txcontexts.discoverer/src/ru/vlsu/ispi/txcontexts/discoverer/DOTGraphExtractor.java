package ru.vlsu.ispi.txcontexts.discoverer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.gmt.modisco.java.AbstractMethodDeclaration;
import org.eclipse.gmt.modisco.java.AbstractTypeDeclaration;
import org.eclipse.gmt.modisco.java.Annotation;
import org.eclipse.gmt.modisco.java.AnnotationMemberValuePair;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.CallNode;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.CallsModel;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.MethodCall;

import ru.vlsu.ispi.txcontexts.analyzer.Analyzer;
import ru.vlsu.ispi.txcontexts.analyzer.ContextSwitch;
import ru.vlsu.ispi.txcontexts.analyzer.SwitchBehavior;
import ru.vlsu.ispi.txcontexts.analyzer.TXContext;
import ru.vlsu.ispi.txcontexts.analyzer.decisions.Cycle;
import ru.vlsu.ispi.txcontexts.analyzer.decisions.Decision;
import ru.vlsu.ispi.txcontexts.analyzer.decisions.Error;
import ru.vlsu.ispi.txcontexts.analyzer.decisions.Redundancy;
import ru.vlsu.ispi.txcontexts.analyzer.decisions.UnspecifiedBehavior;
import ru.vlsu.ispi.txcontexts.discoverer.converter.CallGraphUtils;
import ru.vlsu.ispi.txcontexts.discoverer.converter.TAType;
import ru.vlsu.ispi.txcontexts.discoverer.converter.TMType;
import ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint;
import ru.vlsu.ispi.txcontexts.model.txcontexts.TXContextsModel;

public class DOTGraphExtractor {
	public DOTGraphExtractor(Analyzer a) {
		analyzer = a;
		contextsDiscovered.add(currentContext);
		index  = 1;
	}

	private Analyzer analyzer;

	// исходим из предположения, что точки входа вызываются из пустого контекста
	// транзакции.
	
	private void printCallHistory(){
		List<Map<CallNode,Map<Integer,SwitchBehavior>>> history = analyzer.getCallHistory();
		System.out.println("\n==========CALL HISTORY=========\n");
		//Analyzer.pw.println("\n==========CALL HISTORY=========\n"); Analyzer.pw.flush();
		
		for(Map<CallNode,Map<Integer,SwitchBehavior>> m: history){
			for(CallNode c: m.keySet()){
				System.out.print(c.getName());
			//	Analyzer.pw.print(c.getName()); Analyzer.pw.flush();
				Map<Integer,SwitchBehavior> occurencies = m.get(c);
				for(Integer i: occurencies.keySet()){
					System.out.print("#" + i + " - " + occurencies.get(i));
			//		Analyzer.pw.print("#" + i + " - " + occurencies.get(i)); Analyzer.pw.flush();
				}
			}
			System.out.println();
		//	Analyzer.pw.println();
		}
		System.out.println("\n============================");
	//	Analyzer.pw.println("\n============================"); Analyzer.pw.flush();
	}
	
	private TXContext currentContext = TXContext.NO_TRANSACTION_CONTEXT;

	private List<TXContext> contextsDiscovered = new ArrayList<TXContext>();

	//private HashMap<Integer, Error> currentBranchErrors = new HashMap<Integer, Error>();
	
	private Error currentError = null;
	
	/*
	 * Наверно лучше исходить из общего списка десижонов по каждой EP. А там уж
	 * смотреть, есть ли эрроры или что-то такое.
	 */
	
	private EntryPoint currentEntryPoint = null;
	//private List<Decision> currentEntryPointDecisions = new ArrayList<Decision>();
	private boolean currentEPIsFaulted = false;
	
	public static int index = 1;
	private int currentErrorIndex = -1;
	
	private final int NO_PARENT_INDEX = -1;
	
	//EP -> Всего методов; НефолтныхМетодов;
	//private Map<EntryPoint, List<Integer>> EPMethodCoverage = new LinkedHashMap<EntryPoint, List<Integer>>();
	private int currentEPMethodsTotal = 1;
	private int currentEPMethodsFaulted = 0;
	
	private int currentEPRedundanciesCount = 0;
	private int faultedEPsCount = 0;
	
	//private Error currentError = null;
	/*
	 * Стрингбилдеры для формирования исходников графа
	 */
	private StringBuilder verticesStringBuilder = new StringBuilder("digraph{\nnode[shape=box, style=filled, fillcolor=white];\nedge[label=\"\"];");
	private StringBuilder edgesStringBuilder = new StringBuilder("");
	private StringBuilder TXContextsStringBuilder = new StringBuilder("");
	
	private String errorHighLight = " style=filled fillcolor=lightpink";
	private String unreacheableHighLight = " style=filled fillcolor=lightgrey ";
	private String EPFaultHighLight = "style=filled fillcolor=red";
	
	private String redundancyHighLight = "style=filled; fillcolor=palegoldenrod;";
	private String unspecifiedBehaviorHighLight = " style=filled fillcolor=palegoldenrod ";
	
	private String dashedErrorEdge = ""; // " style=dashed color=red ";
	private String dashedUnreacheableEdge = " style=dashed color=black ";
	private String dashedfilledUnclosedRedundant =  "style=\"dashed, filled, bold\";\ncolor=red;\nfillcolor=yellow;\n";
	private String filledRedundant = "style=\"filled\";\nfillcolor=yellow;\n";
	private String dashedUnclosed = "style=\"dashed, bold\";\ncolor=red;\n";
	

	//private String redundantContextHighLight = "style=filled; fillcolor=yellow;";
	/*
	private boolean isCurrentEPFaulted(){
		for(Decision d: currentEntryPointDecisions){
			if(d instanceof Error){
				return true;
			}
		}
		return false;
	} */
	
	private StringBuilder addDOTVertexElement(CallNode cn, int currentIndex, Set<Decision> decisions, StringBuilder toAdd) {
		Map<TMType, TAType> map = Analyzer.getMetadata().getTypesToTM()
				.get(cn.getJavaMethod().getAbstractTypeDeclaration());
		
		TMType tm = null;
		if(map != null){
			tm = map.keySet().iterator().next();
		}
		toAdd.append("\n").append(currentIndex).append("[label=\"");
		
		TAType ta = Analyzer.getMetadata().getJavaOperationToTA().get(cn.getJavaMethod());
		
		if(tm!=null){
			toAdd.append("@TransactionManagementType(").append(tm).append(")\\n");
			String s = null;
			
			if(analyzer.checkIfCallerHasUnspecifiedBehavior(cn)){
				/* там анализируются всякие ЖЦ 
				 * поэтому если ЖЦ - то это не точка входа
				 */
				if((s=CallGraphUtils.methodIsLifeCycleInterceptor(cn.getJavaMethod()))!=null){
					toAdd.append("@").append(s).append("\\n");
				} else if(CallGraphUtils
						.methodDeclarationHasAnnotationSpecified(cn.getJavaMethod(), "AfterCompletion")){
					toAdd.append("@").append("AfterCompletion").append("\\n");
				}
			}else {
				if(ta!=null){
					toAdd.append("@TransactionAttribute(").append(ta).append(")\\n");
				}
				//System.out.println(cn.getName() + " - нормальная точка входа");
					if(cn.equals(currentEntryPoint.getRootNode())){
						toAdd.append("-Точка входа-\\n");
					}
				}
			}
		
		toAdd.append(cn.getName().replace(" :: ", "\\n"));
		
		String style = "";
		
		int size = decisions.size();
		
		if(size > 0){
			toAdd.append("\\n\\n");
			int i = 0;
			for(Decision d: decisions){
				if(d != null ){
					if(d.getDescription() != null){
						if(d.getDescription().length() > 0){
							toAdd.append(d.getDescription());
							if(i < size - 1){
								toAdd.append("\\n\\n");
							}
						}
					}
					if(!style.equals(unreacheableHighLight)){
						if(d instanceof Error){
									if(d.getDescription().equals(Error.NOT_REACHABLE_METHOD_STRING)){
										style = unreacheableHighLight;
										continue;
									} else if(d.getDescription().equals(Error.EP_ABNORMAL_STOP_STRING)){
										style = EPFaultHighLight;
										continue;
									}
									else{
										style = errorHighLight;
										continue;
									}
								}
							else if(d instanceof Redundancy){
								style = redundancyHighLight;
							} else if(d instanceof UnspecifiedBehavior){
								style = unspecifiedBehaviorHighLight;
						}
					}
				}
				i++;
			}
			toAdd.append("\" ").append(style);
		}
		else{
			toAdd.append("\"");
		}
		
		toAdd.append("]");
		
		
		return toAdd;
	}

	private StringBuilder addDOTEdgeElement(int parentIndex, int currentIndex, Set<Decision> decisions,
			StringBuilder toAdd) {
		// если корневая вершина - ни с чем ее не соединяем.
		// ее потомки с ней соединятся сами.
		String style = "";
		
		if (parentIndex != NO_PARENT_INDEX) {
			toAdd.append(parentIndex).append("->").append(currentIndex);
			if(decisions != null){
			if(decisions.size() > 0){
				for(Decision d: decisions){
					if(d != null){
						if(d instanceof Error){
							if(d.getDescription().equals(Error.NOT_REACHABLE_METHOD_STRING)){
								style = dashedUnreacheableEdge;
								break;
							} else {
								style = dashedErrorEdge;
							}
						} else if(d instanceof Cycle){
							toAdd.append("[label=\"").append(d.getDescription()).append("\"").append(" style=dotted]");
						}
					}
				}
				if(style.length() > 0){
					toAdd.append("[").append(style).append("]");
				}
			}
		}
		} else {
			toAdd.append(currentIndex);
		}
		toAdd.append(";");
		return toAdd;
	}

	private void printMethodAnnotations(CallNode cn) {
		EList<Annotation> anns = cn.getJavaMethod().getAnnotations();
		// TODO Смотреть аннотации у метода из интерфейса, если у текущего
		// метода нет своих аннотаций
		System.out.println("Аннотации у метода " + cn.getName());

		for (Annotation ann : anns) {
			String annName = ann.getType().getType().getName();
			EList<AnnotationMemberValuePair> values = ann.getValues();
			System.out.print(annName + "(");
			for (AnnotationMemberValuePair ap : values) {
				System.out.println("name " + ap.getName());
				if (ap.getMember() != null) {
					System.out.println("member name "
							+ ap.getMember().getName());
				}
				System.out.println("value " + ap.getValue());
			}
			System.out.println(")");
			System.out.println(CallGraphUtils
					.tellWhichTransactionAttributeIsUsed(anns));
		}
	}
/*
	private void putDecisionInEntryPointsToDecisions(Decision decision) {
			currentEntryPointDecisions.add(decision);

			if (decision instanceof Error) {
				System.out.println("Ошибка: "
						+ decision.getDescription());
			}
	} */
/*
	private void addNodeToGraph(CallNode cn, int parentIndex,int currentIndex){
		addDOTVertexElement(cn, currentIndex, verticesStringBuilder, null);
		addDOTEdgeElement(parentIndex, currentIndex, edgesStringBuilder);
	} */
	
	private void addMethodToGraph(CallNode cn, int parentIndex,int currentIndex, Set<Decision> decisions){
		addDOTVertexElement(cn, currentIndex, decisions, verticesStringBuilder);
		addDOTEdgeElement(parentIndex, currentIndex, decisions, edgesStringBuilder);
	}
	
	
	/*
	 * TODO переделать в булеан.
	 * тру - продолдаем, фолс - обнаружн цикл.
	 * 
	 * Добавь в Десижен решение CYCLE_DETECTED
	 * 
	 * Переделать метод вставки - putololoInHistory чтобы возврщал булеан или инт - вставка успешна или нет.
	 * 
	 * Принимать CYCLE_DET регшение, если метод вставки вернул фолс (-1)
	 * 
	 * Если Cycle_detected - то возвразать фолс в этом VVVVV методе
	 */
	private Decision processMethodCall(CallNode callerNode, CallNode calleeNode, int callerIndex, TXContext callerContext){
		//previousContext = currentContext;
		boolean cycleDetected = false;
		
		int currentMethodIndex = ++index;
		
		currentEPMethodsTotal++;
		
		Decision decision = null;
		Set<Decision> currentCallDecisions = new HashSet<Decision>();
		
		/* Самое важное */decision = inspectMethodCall(callerNode, calleeNode, index, callerContext);
		
		if(decision instanceof Cycle){
			cycleDetected = true;
			//currentCallDecisions.add(decision);
		}
		
		if(currentEPIsFaulted){
			System.out.println("----------> Текущая ветка зафолчена!");
			decision = analyzer.callIsProhibited(callerNode, calleeNode);
		} else {
			if(decision != null){
				if(decision instanceof Error){
					System.out.println("Зафолтили куррент ЕП");
					currentEPIsFaulted = true;
					currentErrorIndex = index;
					currentEPMethodsFaulted++;
					faultedEPsCount++;
					//currentContext = TXContext.ERROR_CONTEXT;
				}else if(decision instanceof Redundancy){
					currentEPRedundanciesCount++;
				}
				/*Кладем сюда все решения*/
				//putDecisionInEntryPointsToDecisions(decision);
			}
			
		}
		
		if(decision != null){
			currentCallDecisions.add(decision);
		}
		
		if(!cycleDetected){
			extractNodesRecursively(calleeNode, index);
		} else {
			return decision;
		}
		//methodContext =  currentContext;
		
		
		 if(currentContext.getTmType() == TMType.BEAN){
			System.out.print("БИНОВЫЙ контекст. Пока ничего не делаем. Ждем, когда сам закроется.");
		}
		else{
				currentContext = callerContext;
				System.out.print("Контейнерный контекст. Переключаемся на контекст вызывающего метода.");
				System.out.println(" было стало " + callerContext.getTxId() + " " + currentContext.getTxId());
			}
		 
		if(currentErrorIndex != -1){
			if(currentMethodIndex < currentErrorIndex){
				//помечаем красным восхождение к точке входа
				decision = Error.getNewGenericError();
			} else if (currentMethodIndex > currentErrorIndex){
				decision = Error.getNewNotReacheableMethodError(calleeNode);
				currentEPMethodsFaulted++;
			} else{
				if(decision == null){
					decision = currentError;
				}
			}
		}
		
		if(decision != null){
			currentCallDecisions.add(decision);
		}
		
		addMethodToGraph(calleeNode, callerIndex, currentMethodIndex, currentCallDecisions);
		
		return decision;
	}
	
	private Decision inspectMethodCall(CallNode callerNode, CallNode calleeNode, int calleeIndex, TXContext callerContext){
		
		Decision decision = analyzer.callIsProhibited(callerNode, calleeNode);

		if(decision != null){
			if(decision instanceof Error){
				//putDecisionInEntryPointsToDecisions(decision);
				return decision;
			}
		}
		/*
		AbstractMethodDeclaration callerMethod = null;
		
		//addNodeToGraph(cn, parentIndex, currentIndex, d);
				
		if(callerNode != null){
			callerMethod = callerNode.getJavaMethod();
		} */
		
		ContextSwitch sw = analyzer.inspectTC(callerNode, calleeNode, callerContext);
		
		decision = sw.getDecision();

		if(decision != null){
			if(decision instanceof Error){
				System.out.println("ДА У НАС ТУТ ЭРРОР!");
			}
			if(decision instanceof Cycle){
				return decision;
			}
			//putDecisionInEntryPointsToDecisions(decision);
		}
		
		
		//на случай коммита и ролбэка processMethodCall
		//эти методы я добавляю в Анализаторе и эта проверка нужна чтобы не добавить 2 раза
		boolean methodAlreadyAddedToContext = currentContext.getNodeIndexes().contains(calleeIndex);

		System.out.println("Устанавливаем куррент контекст " + sw.getContext().getTxId());
		currentContext = sw.getContext();
		
		if(!methodAlreadyAddedToContext){
			addMethodAndIndexToContext(currentContext, calleeNode, calleeIndex);
		}
		
		return decision;
	}
		
	
	private void addMethodAndIndexToContext(TXContext context,
			CallNode node, int currentNodeIndex) {
		
			System.out.println("В контекст №" + context.getTxId()
					+ " добавился метод " + node.getName() + " и нода "
					+ currentNodeIndex);
			context.getNodeIndexes().add(currentNodeIndex);
			context.getCallNodesInContext().add(node);
			
			if (!contextsDiscovered.contains(currentContext)) {
				contextsDiscovered.add(currentContext);
			}
		}
	
	/**
	 * Метод, который возвращает исходник графа вызовов в виде строки
	 * @param model
	 * @return
	 */
	public String computeDOTGraph(CallsModel model) {
		TXContext.NO_TRANSACTION_CONTEXT.getCallNodesInContext().clear();
		TXContext.NO_TRANSACTION_CONTEXT.getNodeIndexes().clear();
		
		if (model instanceof TXContextsModel) {
			
			EList<EntryPoint> entryPoints = ((TXContextsModel) model).getEntryPoints();
					
			for (EntryPoint ep : entryPoints) {
				currentEntryPoint = ep;
				
				ep.getRootNode().setName(ep.getRootNode().getName());//????
				
				extractNodesRecursively(ep.getRootNode(), NO_PARENT_INDEX);

				System.out.println("В точке входа " + ep.getRootNode().getName() + " " 
						+ currentEPMethodsTotal + " методов, из них " 
						+ currentEPMethodsFaulted + " недостижимо.\n"
						+ "Найдено " + currentEPRedundanciesCount + " случаев избытосности");
				
				currentEPMethodsTotal = 1;
				currentEPMethodsFaulted = 0;
				currentEPRedundanciesCount = 0;
				
				currentContext = TXContext.NO_TRANSACTION_CONTEXT;
				currentEPIsFaulted = false;
				currentErrorIndex = -1;
				
				index++;
			}
			computeTCSubgraphs();
			System.out.println("В приложении обнаружено " + entryPoints.size() 
					+ " точек входа, из них завершились аварийно " + faultedEPsCount);
		}
		
		// int currentEndNodeIndex = -1;
		return verticesStringBuilder.append("\n").append(TXContextsStringBuilder).append(edgesStringBuilder)
				.append("\n").append("}").toString();//.replaceAll(";;", ";");
	}
	
	private void computeTCSubgraphs(){
		TXContextsStringBuilder = new StringBuilder();
		
		int redundantContextsCount = 0;
		int unclosedContextsCount = 0;
		
		System.out.println("СОДЕРЖИМОЕ КОНТЕКСТОВ");
		for (TXContext tx : contextsDiscovered) {
			int txId = tx.getTxId();
			
			if(txId > 0){
				Redundancy r = analyzer.inspectTCForRedunancy(tx);
				
				String style = "";
				TXContextsStringBuilder.append("subgraph cluster").append(txId -1).append("{\nlabel=\"T").append(txId);
				if(r != null && tx.isUnclosed()){
					TXContextsStringBuilder.append("\\nне закрыт\\n").append("избыточен\\n");
					style = dashedfilledUnclosedRedundant;
					
					unclosedContextsCount++;
					redundantContextsCount++;
				}
				else if(r != null){
					TXContextsStringBuilder.append("\\nизбыточен\\n");//.append("\n");
					style = filledRedundant;
					
					redundantContextsCount++;
					
				} else if(tx.isUnclosed() && tx.getTmType()==TMType.BEAN){
					TXContextsStringBuilder.append("\\nне закрыт\\n");
					style = dashedUnclosed;
					
					unclosedContextsCount++;
				}
				
				TXContextsStringBuilder.append("\";\n");
				if(style.length() > 0){
					TXContextsStringBuilder.append(style);
				}

				System.out.println("КОНТЕКСТ #" + tx.getTxId());
				List<Integer> indexes = tx.getNodeIndexes();
				for (int i : indexes) {
						TXContextsStringBuilder.append(i).append(";");
				}
				TXContextsStringBuilder.append("\n}\n");
			}
		}
		System.out.println("Всего обнаружено " + contextsDiscovered.size() + " транзакционных контекстов," 
				+ " из них избыточно: " + redundantContextsCount + ", не закрыто " + unclosedContextsCount);
		System.out.println(TXContextsStringBuilder.toString());
	}
	
	private void extractNodesRecursively(CallNode callerNode, int parentIndex) {
		int currentMethodIndex = index;
		//CallNode currentCallee = null;
		//System.out.println("В методе " + callerNode.getName() + " индекс " + currentMethodIndex);
		
		AbstractTypeDeclaration callerType = callerNode.getJavaMethod().getAbstractTypeDeclaration();
		Map<TMType, TAType> callerMap = Analyzer.getMetadata().getTypesToTM().get(callerType);
		TMType callerMethodTM = null;
		
		if(callerMap != null){
			callerMethodTM = callerMap.keySet().iterator().next();
		}
		
		Set<Decision> decisions = new HashSet<Decision>();
		Error e = null;
		
		if(parentIndex == NO_PARENT_INDEX){
			//addMethodToGraph(callerNode, NO_PARENT_INDEX, currentMethodIndex, null);

			System.out.println("+++Был контекст " + currentContext.getTxId());
			currentContext = analyzer.inspectTC(null, callerNode, currentContext).getContext();
			System.out.println("Точка входа инициировала контекст " + currentContext.getTxId() + " unspec: " + currentContext.isUnspecified());
			
			addMethodAndIndexToContext(currentContext, callerNode, index);
			
			System.out.println("Управление в контексте " + currentContext.getTxId() + " - " + currentContext.getTmType());
			System.out.println("+++Стал контекст " + currentContext.getTxId() + "\n\n");
			
		}
		
		System.out.println("Вошли в рекурсивный метод у наc стал контекст "  + currentContext.getTxId());
		TXContext methodContext =  currentContext;
		
		//inspectFirstLayerCallees(cn, parentIndex, currentIndex);

		EList<MethodCall> mcs = callerNode.getMethodCalls();

		if (mcs.size() > 0) {
			for (MethodCall mc : mcs) {
				
				CallNode calleeNode = mc.getCallee();
				EList<CallNode> submethods = calleeNode.getSubMethods();
				
				if (submethods.size() > 0) {
					System.out.println("ДА ЕСТЬ СУБМЕТОДЫ");
					for (CallNode submethodNode : submethods) {
						System.out.println("+++Был контекст " + methodContext.getTxId());
						
						Decision potentialCycleDecision = null;
						potentialCycleDecision = processMethodCall(callerNode, submethodNode, currentMethodIndex, methodContext);
						
						if(potentialCycleDecision instanceof Cycle){
							System.out.println("ВОУ ВОУ ВОУ ЦИИИИИИИКЛ! " + calleeNode.getName());
							addCyclicEdge(currentMethodIndex, ((Cycle) potentialCycleDecision).getFirstCallIndex(), potentialCycleDecision);
							continue;
						}
						
						//analyzer.removeCalleeBehaviorFromCallHistory(calleeNode/*, b */);
						
						if(currentContext.getTmType()==TMType.BEAN/* && methodContext==TXContext.NO_TRANSACTION_CONTEXT*/){
							methodContext=currentContext;
						}
						
						System.out.println("Управление в контексте " + currentContext.getTxId() + " - " + currentContext.getTmType());
						System.out.println("+++Стал контекст " + currentContext.getTxId());
					}
				} 
				else {
					System.out.println("+++Был контекст " + methodContext.getTxId());

					Decision potentialCycleDecision = null;
					potentialCycleDecision = processMethodCall(callerNode, calleeNode, currentMethodIndex, methodContext);
					
					if(potentialCycleDecision instanceof Cycle){
						System.out.println("ВОУ ВОУ ВОУ ЦИИИИИИИКЛ! " + calleeNode.getName());
						addCyclicEdge(currentMethodIndex, ((Cycle) potentialCycleDecision).getFirstCallIndex(), potentialCycleDecision);
						continue;
					}
					
					if(currentContext.getTmType()==TMType.BEAN && methodContext==TXContext.NO_TRANSACTION_CONTEXT){
						if(callerMethodTM == TMType.BEAN){
							System.out.println("Распространили начатый БИН контекст на вызывающий метод");
							methodContext=currentContext;
						}
					}
					//analyzer.removeCalleeBehaviorFromCallHistory(calleeNode/*, b */);
					
					System.out.println("Управление в контексте " + currentContext.getTxId() + " - " + currentContext.getTmType());
					System.out.println("+++Стал контекст " + currentContext.getTxId());
				}
			}
			
			
			if(currentContext.getTxId() > 0){
				/* Т.Е. Не пустой и не ошибочный */
				//if(!currentEPIsFaulted){
					if(analyzer.checkForUnclosedTransaction(callerNode, currentContext)){
						Error unclosed = new Error(callerNode, "Незакрытая транзакция в методе\\nStateless-компонента");
						//putDecisionInEntryPointsToDecisions(unclosed);
						
						currentEPIsFaulted = true;
						
						currentErrorIndex = currentMethodIndex;
						//currentBranchErrors.put(currentMethodIndex,unclosed);
						currentError = unclosed;
						currentContext.setUnclosed(true);
						currentContext = TXContext.NO_TRANSACTION_CONTEXT;
						
						//analyzer.getBMBeansToTXContexts().remove(callerNode.getJavaMethod().getAbstractTypeDeclaration());
					}
				//}
			}
			
			if(currentEPIsFaulted){
				e = Error.getNewEPError(callerNode);
				decisions.add(e);
			}
			if(parentIndex == NO_PARENT_INDEX){
				if(e != null){
					decisions.add(e);
				}
				addMethodToGraph(callerNode, NO_PARENT_INDEX, currentMethodIndex, decisions);
			}
		} else if(parentIndex == NO_PARENT_INDEX) {
			if(e != null){
				decisions.add(e);
			}
			addMethodToGraph(callerNode, NO_PARENT_INDEX, ++index, decisions);
			/*
			if(!currentContext.getNodeIndexes().contains(index)){
				addMethodAndIndexToContext(currentContext, callerNode, index);
			}
			*/
		}
		printCallHistory();
		
		analyzer.removeLastCalleeBehaviorFromCallHistory(callerNode/*, b */);
	}
	
	private void addCyclicEdge(int parentIndex, int currentIndex, Decision cycle){
		Set<Decision> s = new HashSet<Decision>();
		s.add(cycle);
		addDOTEdgeElement(parentIndex, currentIndex, s, edgesStringBuilder);
	}
}
