package ru.vlsu.ispi.txcontexts.discoverer.converter;

import java.util.ArrayList;

import org.eclipse.emf.common.util.EList;
import org.eclipse.gmt.modisco.java.AbstractMethodDeclaration;
import org.eclipse.gmt.modisco.java.AbstractTypeDeclaration;
import org.eclipse.gmt.modisco.java.Annotation;
import org.eclipse.gmt.modisco.java.AnnotationMemberValuePair;
import org.eclipse.gmt.modisco.java.TypeAccess;
import org.eclipse.gmt.modisco.java.TypeDeclaration;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.CallNode;

import ru.vlsu.ispi.txcontexts.analyzer.Analyzer;

public class CallGraphUtils {
	public static String computeFullyQualifiedTypeName() {
		return null;
	}
	
	private static final String[] SSBUnspecifiedTCLifecycleAnnotations ={
			"PostConstruct", "PreDestroy"
	};
	private static final String[] SFSBUnspecifiedTCLifecycleAnnotations ={
			"PostConstruct", "PreDestroy", "PostActivate", "PrePassivate"
	};
	
	

	public static TAType tellWhichTransactionAttributeIsUsed(EList<Annotation> annotations) {
		outer: for (Annotation ann2 : annotations) {
			if (ann2.getType().getType().getName()
					.contains("TransactionAttribute")) {
				for (AnnotationMemberValuePair ap2 : ann2.getValues()) {
					if (ap2.getValue().toString().contains("name: REQUIRED")) {
						return TAType.REQUIRED;
					}
					if (ap2.getValue().toString()
							.contains("name: REQUIRES_NEW")) {
						return TAType.REQUIRES_NEW;
					}
					if (ap2.getValue().toString().contains("name: MANDATORY")) {
						return TAType.MANDATORY;
					}
					if (ap2.getValue().toString().contains("name: SUPPORTS")) {
						return TAType.SUPPORTS;
					}
					if (ap2.getValue().toString().contains("name: NEVER")) {
						return TAType.NEVER;
					}
					if (ap2.getValue().toString()
							.contains("name: NOT_SUPPORTED")) {
						return TAType.NOT_SUPPORTED;
					}
				}
			}

		}
		return null;
	}
	
	public static boolean typeDeclarationHasAnnotationSpecified(AbstractTypeDeclaration type, String annotationName){
		System.out.println("Проверяем на наличие аннотации " + annotationName);
		for(Annotation ann: type.getAnnotations()){
			System.out.print(ann.getType().getType().getName() + "");
			if(ann.getType().getType().getName().contains(annotationName)){
				return true;
			}
		}
		return false;
	}
	
	public static boolean methodDeclarationHasAnnotationSpecified(AbstractMethodDeclaration method, String annotationName){
		for(Annotation ann: method.getAnnotations()){
			if(ann.getType().getType().getName().contains(annotationName)){
				return true;
			}
		}
		return false;
	}
	
	public static boolean methodIsLifeCycleCallbackWithUnspecifiedBehavior(CallNode node){
			return methodIsLifeCycleCallbackWithUnspecifiedBehavior(node.getJavaMethod());
	}
	
	
	public static boolean methodIsLifeCycleCallbackWithUnspecifiedBehavior(AbstractMethodDeclaration method){
		AbstractTypeDeclaration type = method.getAbstractTypeDeclaration();
		ComponentKind kind = Analyzer.getMetadata().getTypeToComponentKind().get(type);
		
		switch(kind){
		case EJB_STATELESS:
			for(String s: SSBUnspecifiedTCLifecycleAnnotations){
				if(CallGraphUtils.methodDeclarationHasAnnotationSpecified(method, s)){
					System.out.println("У метода " + method.getName() + " нашли аннотацию " + s + " а это неопр конт");
					return true;
				}
			}
		case EJB_STATEFUL:
			for(String s: SFSBUnspecifiedTCLifecycleAnnotations){
				if(CallGraphUtils.methodDeclarationHasAnnotationSpecified(method, s)){
					System.out.println("У метода " + method.getName() + " нашли аннотацию " + s + " а это неопр конт");
					return true;
				}
			}
		default: 
			return false;
	}
}
	
	public static String methodIsLifeCycleInterceptor(AbstractMethodDeclaration method){
		AbstractTypeDeclaration type = method.getAbstractTypeDeclaration();
		ComponentKind kind = Analyzer.getMetadata().getTypeToComponentKind().get(type);
		
		switch(kind){
		case EJB_STATELESS:
		case EJB_SINGLETON:
			for(String s: SSBUnspecifiedTCLifecycleAnnotations){
				if(CallGraphUtils.methodDeclarationHasAnnotationSpecified(method, s)){
					return s;
				}
			}
		case EJB_STATEFUL:
			for(String s: SFSBUnspecifiedTCLifecycleAnnotations){
				if(CallGraphUtils.methodDeclarationHasAnnotationSpecified(method, s)){
					return s;
				}
			}
		default: 
			return null;
	}
}
	
	public static boolean typeIsSubtypeOf(AbstractTypeDeclaration type, String superTypeName){
		String tmp = null;
		
		for (TypeAccess superTypeAccess : type.getSuperInterfaces()) {
			if (superTypeAccess.getType() instanceof TypeDeclaration) {
				TypeDeclaration superType = (TypeDeclaration) superTypeAccess.getType();
				
				tmp = addPackagesToName(superType.getName(), superType.getPackage());
				
				System.out.println("Тип " + tmp + " и Cупертип " + superTypeName);
				if (tmp.equals(superTypeName)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public static String addPackagesToName(String name,
			org.eclipse.gmt.modisco.java.Package pack) {

		ArrayList<String> packNames = new ArrayList<String>();

		String fullyQualifiedName = "";

		while (pack != null) {

			packNames.add(pack.getName());

			pack = pack.getPackage();
		}

		for (int i = packNames.size() - 1; i > -1; i--) {
			fullyQualifiedName = fullyQualifiedName + packNames.get(i) + ".";
		}

		fullyQualifiedName = fullyQualifiedName + name;

		/*
		 * Рекурсивная ерунда String tmp = "";
		 * 
		 * if(pack.getPackage() == null){ tmp = pack.getName(); } else{ pack =
		 * pack.getPackage(); tmp = addPackagesToName(tmp, pack) +
		 * pack.getName(); tmp += "." + name; }
		 */

		/*
		 * while(pack != null){ System.out.println("пакет " + pack.getName());
		 * pack = pack.getPackage(); }
		 * 
		 * EList<org.eclipse.gmt.modisco.java.Package> ps =
		 * pack.getOwnedPackages();
		 * 
		 * int l = ps.size(); System.out.println("размер массива " + l);
		 * 
		 * for(org.eclipse.gmt.modisco.java.Package p: ps){ name = p.getName() +
		 * '.' + name; }
		 * 
		 * if(atd.getPackage().getName().length() == 0 ){ name = name + "." +
		 * atd.getPackage().getName(); } else{ name += addPackagesToName(name,
		 * atd); }
		 */
		return fullyQualifiedName;
	}
}
