package ru.vlsu.ispi.txcontexts.discoverer.converter;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.gmt.modisco.java.AbstractMethodDeclaration;
import org.eclipse.gmt.modisco.java.AbstractTypeDeclaration;

import ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint;

public class J2EECallGraphMetadata {
	
	private Map<AbstractTypeDeclaration, Map<TMType, TAType>> typesToTM = new HashMap<AbstractTypeDeclaration, Map<TMType, TAType>>();
	private Map<AbstractMethodDeclaration, EntryPoint> javaOperationToEntryPoint = new HashMap<AbstractMethodDeclaration, EntryPoint>();
	private Map<AbstractTypeDeclaration, ComponentKind> typeToComponentKind = new HashMap<AbstractTypeDeclaration, ComponentKind>();
	private Map<AbstractMethodDeclaration, TAType> javaOperationToTA = new HashMap<AbstractMethodDeclaration, TAType>();
	
	public Map<AbstractMethodDeclaration, TAType> getJavaOperationToTA() {
		return javaOperationToTA;
	}

	public void setJavaOperationToTA(
			Map<AbstractMethodDeclaration, TAType> javaOperationToTA) {
		this.javaOperationToTA = javaOperationToTA;
	}

	public Map<AbstractTypeDeclaration, Map<TMType, TAType>> getTypesToTM(){
		return typesToTM;
	}
	
	public Map<AbstractMethodDeclaration, EntryPoint> getJavaOperationToEntryPoint(){
		return javaOperationToEntryPoint;
	}

	public Map<AbstractTypeDeclaration, ComponentKind> getTypeToComponentKind() {
		return typeToComponentKind;
	}

	public void setTypeToComponentKind(
			Map<AbstractTypeDeclaration, ComponentKind> typeToComponentKind) {
		this.typeToComponentKind = typeToComponentKind;
	}

	public void setTypesToTM(Map<AbstractTypeDeclaration, Map<TMType, TAType>> typesToTM) {
		this.typesToTM = typesToTM;
	}

	public void setJavaOperationToEntryPoint(
			Map<AbstractMethodDeclaration, EntryPoint> javaOperationToEntryPoint) {
		this.javaOperationToEntryPoint = javaOperationToEntryPoint;
	}
	
	
}
