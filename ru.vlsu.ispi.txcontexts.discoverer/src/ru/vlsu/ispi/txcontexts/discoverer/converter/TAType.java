package ru.vlsu.ispi.txcontexts.discoverer.converter;


public enum TAType {
	REQUIRED, REQUIRES_NEW, MANDATORY, SUPPORTS, NEVER, NOT_SUPPORTED; 
}
