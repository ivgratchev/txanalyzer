/**
 * Copyright (c) 2010, 2011 Mia-Software.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Gabriel Barbier (Mia-Software) - initial API and implementation
 *    Fabien Giquel (Mia-Software) - Bug 339720 : MoDisco Discoverers (infra + techno) API clean
 *    Nicolas Bros (Mia-Software) - Bug 335003 - [Discoverer] : Existing Discoverers Refactoring based on new framework
 *******************************************************************************/

package ru.vlsu.ispi.txcontexts.discoverer.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gmt.modisco.java.AbstractMethodDeclaration;
import org.eclipse.gmt.modisco.java.AbstractMethodInvocation;
import org.eclipse.gmt.modisco.java.AbstractTypeDeclaration;
import org.eclipse.gmt.modisco.java.Annotation;
import org.eclipse.gmt.modisco.java.AnnotationMemberValuePair;
import org.eclipse.gmt.modisco.java.Assignment;
import org.eclipse.gmt.modisco.java.Block;
import org.eclipse.gmt.modisco.java.BodyDeclaration;
import org.eclipse.gmt.modisco.java.ClassDeclaration;
import org.eclipse.gmt.modisco.java.ClassInstanceCreation;
import org.eclipse.gmt.modisco.java.Expression;
import org.eclipse.gmt.modisco.java.InterfaceDeclaration;
import org.eclipse.gmt.modisco.java.MethodInvocation;
import org.eclipse.gmt.modisco.java.SingleVariableAccess;
import org.eclipse.gmt.modisco.java.SingleVariableDeclaration;
import org.eclipse.gmt.modisco.java.TypeAccess;
import org.eclipse.gmt.modisco.java.TypeDeclaration;
import org.eclipse.gmt.modisco.java.VariableDeclaration;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.CallNode;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.MethodCall;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.MethodcallsFactory;

import ru.vlsu.ispi.txcontexts.discoverer.ejb.EjbJarExplorer;
import ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint;
import ru.vlsu.ispi.txcontexts.model.txcontexts.TXContext;
import ru.vlsu.ispi.txcontexts.model.txcontexts.TXContextsModel;
import ru.vlsu.ispi.txcontexts.model.txcontexts.TXKind;
import ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsFactory;

/**
 * From a java model, we will extract method calls informations to initialise a
 * TX contexts model.
 */
public class TXContextsGraphConverter {

	/**
	 * Component kind is determined by annotations analysis and is used to
	 * identify entry points.
	 * 
	 * @author ivgratchev
	 * 
	 *         import javax.ejb.TransactionManagement; import
	 *         javax.ejb.TransactionManagementType;
	 * @TransactionManagement(TransactionManagementType.CONTAINER)
	 * 
	 *                                                             import
	 *                                                             javax.ejb.
	 *                                                             TransactionAttribute
	 *                                                             ; import
	 *                                                             javax.ejb.
	 *                                                             TransactionAttributeType
	 *                                                             ;
	 * @TransactionAttribute(TransactionAttributeType.REQUIRED)
	 * 
	 */
	
	public TXContextsGraphConverter(){
		System.out.println(ejbNamesToClasses + " " + classNamesToKinds + " " + classNamesToTMs + " " + methodsToTAs);
	}

	private Map<String, String> ejbNamesToClasses;
    private Map<String, String> classNamesToTMs;
    private Map<String, String> classNamesToKinds;
    private Map<String, String> methodsToTAs;

	private final MethodcallsFactory factory = MethodcallsFactory.eINSTANCE;
	private final TxcontextsFactory txfactory = TxcontextsFactory.eINSTANCE;
	private final Map<AbstractMethodDeclaration, CallNode> javaOperationToCallNode = new HashMap<AbstractMethodDeclaration, CallNode>();
	private final List<TypeDeclaration> allTypes = new ArrayList<TypeDeclaration>();

	// Cтавим в соответствие типам их способ управления: BEAN / CONTAINER
	private final Map<AbstractTypeDeclaration, Map<TMType, TAType>> typesToTM = new HashMap<AbstractTypeDeclaration, Map<TMType, TAType>>();

	private final Map<AbstractMethodDeclaration, EntryPoint> javaOperationToEntryPoint = new HashMap<AbstractMethodDeclaration, EntryPoint>();
	private final Map<AbstractMethodDeclaration, TAType> javaOperationToTA = new HashMap<AbstractMethodDeclaration, TAType>();

	private final Map<AbstractTypeDeclaration, ComponentKind> typeToComponentKind = new HashMap<AbstractTypeDeclaration, ComponentKind>();

	public TXContextsModel convertJavaResourceToTXContextsModel(
			final Resource javaResource, final IFile ejbjarResource,
			final String targetModelName) {
		getEJBs(ejbjarResource);

		// преобразуем ява ресурс в модель транз контекстов.
		return convertJavaResourceToTXContextsModel(javaResource,
				targetModelName);
	}

	private final List<AbstractMethodInvocation> allInvocations = new ArrayList<AbstractMethodInvocation>();

	/**
	 * Главный супер-метод. From a java resource, we will compute method calls
	 * graph then initialize a method calls model.
	 * 
	 * @param javaResource
	 * @return corresponding method calls model
	 */

	

	private String findParameters(AbstractMethodDeclaration javaOperation) {
		StringBuilder result = new StringBuilder("(");
		// String result = "(";

		EList<SingleVariableDeclaration> parameters = javaOperation
				.getParameters();

		for (int i = 0; i < parameters.size(); i++) {

			// System.out.println("Found parameters " + parameter.toString());
			String paramTypeName = parameters.get(i).getType().getType()
					.getName();
			// System.out.println("Parameter type " + paramTypeName);

			typesIterator: {
				for (TypeDeclaration td : allTypes) {
					if (td.getName().equals(paramTypeName)) {
						result.append(CallGraphUtils.addPackagesToName(paramTypeName,
								td.getPackage()));
						break typesIterator;
					}
				}
				result.append((paramTypeName));
			}

			if (i != parameters.size() - 1) {
				result.append(",");
			}
		}

		result.append(")");
		return result.toString();
	}

	public TXContextsModel convertJavaResourceToTXContextsModel(
			final Resource javaResource, final String targetModelName) {

		/*
		 * Видать, какой-то сгенерированный по емф-модели класс фабрики Create
		 * method calls model root
		 */
		TXContextsModel callsModel = this.txfactory.createTXContextsModel();
		callsModel.setName(targetModelName);

		// получили список объявлений (тел?) методов
		List<AbstractMethodDeclaration> allOperations = getAllOperations(javaResource);

		/*
		 * напихиваем найденные описания методов в узлы модели callsModel. 1
		 * метод - 1 узел
		 */
		for (AbstractMethodDeclaration javaOperation : allOperations) {
			/*
			 * create calls node
			 */
			CallNode callNode = this.factory.createCallNode();
			callNode.setJavaMethod(javaOperation);
			callsModel.getCallNodes().add(callNode);

			// если метод ниоткуда не вызывается - считаем его корневым (точкой
			// входа?)
			if (javaOperation.getUsages().isEmpty()) {
				callsModel.getRootNodes().add(callNode);

				// теперь получаем ТИП ПО МЕТОДУ, ИЗ КОТОРОГО ОН ВЫЗВАН
				AbstractTypeDeclaration parent = javaOperation
						.getAbstractTypeDeclaration();

				// типы уже заполнены.
				System.out.println("Тип из перента " + typeToComponentKind.get(parent));
				switch (typeToComponentKind.get(parent)) {
					case EJB_STATEFUL:
					case EJB_STATELESS:
					case EJB_SINGLETON:
					case MANAGED_BEAN:
					/*
					 * опять самопальная фабрика точек входа. сделали
					 * невызываемый метод корневым узлом.
					 */
					EntryPoint entryPoint = this.txfactory.createEntryPoint();
					entryPoint.setRootNode(callNode);

					String parentName = ""; //$NON-NLS-1$
					if (parent != null) {
						parentName = parent.getName();
					}
					// нашли строчку: Тип::имяМетода()
					String name = parentName + " :: " + javaOperation.getName(); //$NON-NLS-1$
					// дали точке входа имя Тип::имяМетода()
					entryPoint.setName(name);

					/*
					 * TXContext ctx = this.txfactory.createTXContext();
					 * ctx.setKind(typeToComponentKind.get(parent) ==
					 * ComponentKind.EJB ? TXKind.JTA : TXKind.NONE);
					 * entryPoint.getContexts().add(ctx);
					 * entryPoint.getContextChain().add(ctx);
					 */

					// Добавили точку входа во множество точек входа на модели
					// callsModel
					callsModel.getEntryPoints().add(entryPoint);

					/*
					 * Какой-то особый мэп, операция -> точка входа.
					 */
					this.javaOperationToEntryPoint.put(javaOperation,
							entryPoint);
					break;

				case EJB_INTERFACE:
				case ENTITY:
				case NONE:
					break;
				}
			}
			/*
			 * как разновидность, мэп метод -> нода.
			 */
			this.javaOperationToCallNode.put(javaOperation, callNode);
		}

		/*
		 * iterate over all operations to compute method calls information
		 * 
		 * Тут присваиваем нодам имена.
		 */
		for (AbstractMethodDeclaration javaOperation : allOperations) {
			CallNode callNode = this.javaOperationToCallNode.get(javaOperation);
			// EntryPoint entryPoint =
			// this.javaOperationToEntryPoint.get(javaOperation);
			// TXContext currentContext = entryPoint != null ?
			// entryPoint.getContextChain().get(0) : null;

			/*
			 * 
			 * ПОЛУЧИЛИ СПИСОК МЕТОДОВ, ВЫЗВАННЫХ ИЗ ДАННОГО МЕТОДА
			 */
			List<AbstractMethodInvocation> calledMethods = getCalledMethods(javaOperation);
			/*
			 * Compute call node name
			 */
			String parentName = ""; //$NON-NLS-1$
			if (javaOperation.getAbstractTypeDeclaration() != null) {
				/*
				 * parentName =
				 * javaOperation.getAbstractTypeDeclaration().getPackage()
				 * .getName()+"."+javaOperation.getAbstractTypeDeclaration()
				 * .getName();
				 */
				parentName = CallGraphUtils.addPackagesToName(javaOperation
						.getAbstractTypeDeclaration().getName(), javaOperation
						.getAbstractTypeDeclaration().getPackage());

			}
			String name = parentName
					+ " :: " + javaOperation.getName() + findParameters(javaOperation); //$NON-NLS-1$
			/*
			 * 
			 * Клевая фича: для каждой CallNode мы поменяли имя в соответствии с
			 * Тип::имяМетода(сколько_методов_из_него_вызвано) закомментил ибо
			 * чушь
			 */
			if (!calledMethods.isEmpty()) {
				//name = name + " (" + calledMethods.size() + ")"; //$NON-NLS-1$ //$NON-NLS-2$
			}
			callNode.setName(name);

			/*
			 * initialize method call elements Для каждого вызываемого метода
			 * находим целый веер методов с такой же сигнатурой на подтипах. Это
			 * называется MethodCall.
			 * 
			 * И к ноде, представляющей данный метод, добавдяем по одному такому
			 * вееру на каждый callNode.getMethodCalls().add(methodCall);
			 * вызываемый метод.
			 */
			for (int index = 0; index < calledMethods.size(); index++) {

				MethodCall methodCall = this.factory.createMethodCall();
				methodCall.setOrder(index);

				CallNode callee = this.javaOperationToCallNode
						.get(calledMethods.get(index).getMethod());
				methodCall.setCallee(callee);

				/*
				 * Для каждого метода получили список вызываемых методов Создали
				 * MethodCall - что это? Сущность графа вызовов? Потом получили
				 * переменную типа, на котором вызван метод.
				 * 
				 * Для переменной типа нашли типы-реализации. Для типов
				 * реализаций - тела методов, которые могут быть вызваны.
				 * Скрупулезно подсчитали имя и количество параметров.
				 * 
				 * Итеративно получаем CallNode для каждого из этих методов из
				 * классов реализаторов. Добавляем каждую ноду найденный метод в
				 * methodCall.getFilteredSubMethods().add(subNode);
				 * 
				 * 
				 * compute filtered methods 1. we have to retrieve variable
				 * declaration which contains this method invocation, if it
				 * exists. 2. get collection of filtered potential types
				 */
				VariableDeclaration variableDeclaration = getVariableDeclaration(calledMethods
						.get(index));
				if (variableDeclaration != null) {
					for (TypeDeclaration subtype : getFilteredPotentialTypes(variableDeclaration)) {
						/*
						 * we have to retrieve corresponding method same name,
						 * same number of parameters same name of parameters ?
						 * -> may change same type of parameters ? may be a
						 * subtype
						 */
						for (BodyDeclaration body : subtype
								.getBodyDeclarations()) {
							if (body instanceof AbstractMethodDeclaration) {
								AbstractMethodDeclaration subMethod = (AbstractMethodDeclaration) body;
								if (javaOperation.getName().equals(
										body.getName())) {
									if (javaOperation.getParameters().size() == subMethod
											.getParameters().size()) {
										CallNode subNode = this.javaOperationToCallNode
												.get(subMethod);
										System.out
												.println("Добавляем филтеред");
										methodCall.getFilteredSubMethods().add(
												subNode);
									}
								}
							}
						}
					}
				}
				/*
				 * if everything is ok, we could add this method call to current
				 * call node element.
				 */
				callNode.getMethodCalls().add(methodCall);

				/*
				 * if (currentContext != null)
				 * currentContext.getCalls().add(methodCall);
				 */
			}
			/*
			 * initialize collection of sub methods
			 */

			TypeDeclaration parentType = getTypeDeclaration(javaOperation);
			for (TypeDeclaration subtype : getAllSubTypes(parentType)) {
				/*
				 * we have to retrieve corresponding method same name, same
				 * number of parameters same name of parameters ? -> may change
				 * same type of parameters ? may be a subtype
				 */

				/*
				 * Не знаю, зачем это надо, но мы зачем-то добавляем методы из
				 * подтипов как cубноды. И так для каждой ноды из списка
				 * найденных ОПРЕДЕЛЕНИЙ методов. CallNode subNode =
				 * javaOperationToCallNode.get(subMethod);
				 * callNode.getSubMethods().add(subNode);
				 */
				for (BodyDeclaration body : subtype.getBodyDeclarations()) {
					if (body instanceof AbstractMethodDeclaration) {
						AbstractMethodDeclaration subMethod = (AbstractMethodDeclaration) body;
						if (javaOperation.getName().equals(body.getName())) {
							if (javaOperation.getParameters().size() == subMethod
									.getParameters().size()) {
								CallNode subNode = this.javaOperationToCallNode
										.get(subMethod);
								callNode.getSubMethods().add(subNode);
							}
						}
					}
				}
			}
			addJavaOperationToTAEntry(javaOperation);
		}
		System.out.println("ТАААК, где у нас какие методы");

		for (AbstractMethodDeclaration amd : javaOperationToTA.keySet()) {
			System.out.println(amd.getName() + " " + javaOperationToTA.get(amd));
		}

		/**
		 * Для каждого entryPoint определяем тип этого entryPoint (EJB, Managed
		 * и т.п.) создаем через кустарную фабрику котекст транзакции. В
		 * зависимости от типа инициализируем (TXKind.JTA : TXKind.NONE)
		 */
		for (EntryPoint entryPoint : callsModel.getEntryPoints()) {
			TXContext ctx = this.txfactory.createTXContext();
			CallNode rootNode = entryPoint.getRootNode();
			ctx.setKind((typeToComponentKind.get(getTypeDeclaration(rootNode.getJavaMethod())) == ComponentKind.EJB_SINGLETON
					|| typeToComponentKind.get(getTypeDeclaration(rootNode.getJavaMethod())) == ComponentKind.EJB_STATEFUL 
					|| typeToComponentKind.get(getTypeDeclaration(rootNode.getJavaMethod())) == ComponentKind.EJB_STATELESS) ? TXKind.JTA
					: TXKind.NONE);

			/*
			 * 
			 * ctx.setKind(typeToComponentKind.get(getTypeDeclaration((
			 * rootNode.getJavaMethod())) == ComponentKind.EJB_STATEFUL ||
			 * rootNode.getJavaMethod()) == ComponentKind.EJB_STATELESS ||
			 * rootNode.getJavaMethod()) == ComponentKind.EJB_SINGLETON) ?
			 * TXKind.JTA : TXKind.NONE);
			 */

			entryPoint.getContexts().add(ctx);
			entryPoint.getContextChain().add(ctx);

			/*
			 * Добавляем из rootNode веера methodCalls в контекст.
			 */
			//recursiveAddMethodCallsToContext(ctx, rootNode);
		}

		return callsModel;
	}

	private TAType getTAFromString(String TAString){
		if(TAString.equalsIgnoreCase("Required")){
			return TAType.REQUIRED;
		} else if (TAString.equalsIgnoreCase("RequiresNew")){
			return TAType.REQUIRES_NEW;
		}else if (TAString.equalsIgnoreCase("Supports")){
			return  TAType.SUPPORTS;
		}else if (TAString.equalsIgnoreCase("NotSupported")){
			return  TAType.NOT_SUPPORTED;
		}else if (TAString.equalsIgnoreCase("Mandatory")){
			return  TAType.MANDATORY;
		}else if (TAString.equalsIgnoreCase("Never")){
			return  TAType.NEVER;
		}
		return null;
	}
	
	private void addJavaOperationToTAEntry(AbstractMethodDeclaration javaOperation) {
		ComponentKind ck = typeToComponentKind.get(javaOperation
				.getAbstractTypeDeclaration());
		
		
		String typeName = CallGraphUtils.addPackagesToName(
				javaOperation.getAbstractTypeDeclaration().getName(), 
				javaOperation.getAbstractTypeDeclaration().getPackage());
		
		String parameters = findParameters(javaOperation);
		String methodName = typeName + " :: " + javaOperation.getName() + parameters;
		System.out.println("ejb - jar Проверяем ТА у метода " + methodName);
		
		if(methodsToTAs.containsKey(methodName)){
			String TA = methodsToTAs.get(methodName);
			javaOperationToTA.put(javaOperation, getTAFromString(TA));
			System.out.println("Нашли " + getTAFromString(TA));
			return;
		} else {
			methodName = typeName + " :: *";
				if(methodsToTAs.containsKey(methodName)){
				String TA = methodsToTAs.get(methodName);
				javaOperationToTA.put(javaOperation, getTAFromString(TA));
				System.out.println("Нашли из звездочки " + getTAFromString(TA));
				return;
			}
		} 

		if (ck == ComponentKind.EJB_SINGLETON
			|| ck == ComponentKind.EJB_STATEFUL
			|| ck == ComponentKind.EJB_STATELESS) {
			
			TAType ta = CallGraphUtils
					.tellWhichTransactionAttributeIsUsed(javaOperation
							.getAnnotations());

			if (ta == null) {
				AbstractTypeDeclaration parentType = javaOperation
						.getAbstractTypeDeclaration();
				// System.out.println("Размер злосчастного мэпа " +
				// typesToTM.size());
				if (typesToTM.containsKey(parentType)) {
					for (TMType tm : typesToTM.get(parentType).keySet()) {
						ta = typesToTM.get(parentType).get(tm);
						break;
					}
				}
			}
			// помещаем в соответствие javaOperationToTA только методы с
			// декларативным управлением, т.е. где этот самый ТА не нулл.
			if (ta != null) {
				javaOperationToTA.put(javaOperation, ta);
			}
		}
	}

	/**
	 * Рекурсивно добавляем ноды в контекст - заваливало модель в стек оверфлоу
	 
	private void recursiveAddMethodCallsToContext(TXContext ctx,
			CallNode rootNode) {
		for (MethodCall call : rootNode.getMethodCalls()) {
			ctx.getCalls().add(call);
			recursiveAddMethodCallsToContext(ctx, call.getCallee());
		}
	}*/

	/**
	 * To retrieve all method declarations from java model.
	 * 
	 * We will also initialize collections of types and method invocations to
	 * iterate only once on all java elements.
	 * 
	 * Клевый метод, в котором проходим по всему дереву ресурсов (вроде в
	 * предыдущем обнаружителе они модифицировались? Да, добавлялось
	 * getResourceSet().getResources().add(javaModel))
	 * 
	 * Заполняем сразу лист с элементами объявления
	 * List<AbstractMethodDeclaration> result, лист с элементами вызова методов
	 * List<AbstractMethodInvocation> allInvocations лист с найденными типами
	 * проекта List<TypeDeclaration> allTypes
	 * 
	 * Также определяем тип компонента и засовываем его в Map<AbsTypDec,
	 * ТипКомпонента>
	 * 
	 * @param javaResource
	 * @return
	 * 
	 */

	private final List<AbstractMethodDeclaration> getAllOperations(
			final Resource javaResource) {

		// под операциями понимается декларация методов
		List<AbstractMethodDeclaration> result = new ArrayList<AbstractMethodDeclaration>();

		TreeIterator<EObject> iterator = javaResource.getAllContents();

		while (iterator.hasNext()) {
			EObject object = iterator.next();

			if (object instanceof AbstractMethodDeclaration) {
				// под операциями понимается декларация методов
				AbstractMethodDeclaration operation = (AbstractMethodDeclaration) object;
				result.add(operation);
			}

			else if (object instanceof AbstractMethodInvocation) {
				AbstractMethodInvocation invocation = (AbstractMethodInvocation) object;
				this.allInvocations.add(invocation);
			}

			else if (object instanceof TypeDeclaration) {
				TypeDeclaration currentClassDeclaration = (TypeDeclaration) object;
				allTypes.add(currentClassDeclaration);

				ComponentKind kind = ComponentKind.NONE;
				// TMType tm = TMType.CONTAINER;

				boolean isInterface = currentClassDeclaration instanceof InterfaceDeclaration;
				
				//EJBJAR
				
				String className = CallGraphUtils.addPackagesToName(currentClassDeclaration.getName(), currentClassDeclaration.getPackage());
				System.out.println("Сlass name " + className);
				
				if(classNamesToKinds.containsKey(className)){
					String kindString = classNamesToKinds.get(className);
					System.out.println("kindString " + kindString);
					if (kindString.equals("Stateless")) {
						System.out.println("Нашли стейтлес компонент " + currentClassDeclaration.getName());
						kind = ComponentKind.EJB_STATELESS;
					} else if (kindString.equals("Stateful")) {
						System.out.println("Нашли стейтфул компонент " + currentClassDeclaration.getName());
						kind = ComponentKind.EJB_STATEFUL;
					} else if (kindString.equals("Singleton")) {
						System.out.println("Нашли singleton компонент " + currentClassDeclaration.getName());
						kind = ComponentKind.EJB_SINGLETON;
					}
				}
				else{
					for (Annotation annotation : currentClassDeclaration
							.getAnnotations()) {
						String annotationType = annotation.getType().getType()
								.getName();
						System.out.println("Annotation found: " + annotationType);
						// annotation type name contains only local part
						// (maybe only if annotation type is imported)
	
						if (annotationType.contains("Stateless")) {
							kind = ComponentKind.EJB_STATELESS;
							continue;
						} else if (annotationType.contains("Stateful")) {
							//System.out.println("Нашли стейтфул компонент " + currentClassDeclaration.getName());
							kind = ComponentKind.EJB_STATEFUL;
							continue;
						} else if (annotationType.contains("Singleton")) {
							kind = ComponentKind.EJB_SINGLETON;
							continue;
						} else if (annotationType.contains("Entity")) {
							kind = ComponentKind.ENTITY;
							continue;
						} else if (annotationType.contains("ManagedBean")) {
							kind = ComponentKind.MANAGED_BEAN;
							continue;
						} else if (isInterface
								&& (annotationType.contains("Local") || annotationType
										.contains("Remote"))) {
							kind = ComponentKind.EJB_INTERFACE;
							continue;
						}
					}
				}
				typeToComponentKind.put(currentClassDeclaration, kind);
				putTypeInTypeToTMMap(currentClassDeclaration);
			}
		}
		System.out.println("~~~Компоненты и их управление~~~");
		for (AbstractTypeDeclaration td : typesToTM.keySet()) {
			System.out.println(td.getName() + " " + typesToTM.get(td));
			/*
			 * for(TMType tm : typesToTM.get(td).keySet()){
			 * System.out.println(tm + " " + typesToTM.get(td).get(tm)); }
			 */
		}
		return result;
	}

	private void putTypeInTypeToTMMap(TypeDeclaration td) {
		System.out.println("Внутри putTypeInTypeToTMMap");
		//EJB-JAR
		HashMap<TMType, TAType> TMDetails = null;
		String className = CallGraphUtils.addPackagesToName(td.getName(), td.getPackage());
		System.out.println("Класс нейм " + className);
		
		if(classNamesToTMs.containsKey(className)){
			TMDetails = new HashMap<TMType, TAType>();
			if(classNamesToTMs.get(CallGraphUtils.addPackagesToName(td.getName(), td.getPackage())).equalsIgnoreCase("Bean")){
				System.out.println("Нашли через дескриптор, что тип - БИН");
				TMDetails.put(TMType.BEAN, null);
				typesToTM.put(td, TMDetails);
				return;
			}
		}
		
		for (Annotation ann : td.getAnnotations()) {
			if (ann.getType().getType().getName()
					.contains("TransactionManagement")) {
				for (AnnotationMemberValuePair ap : ann.getValues()) {
					if (ap.getValue().toString().contains("BEAN")) {
						TMDetails = new HashMap<TMType, TAType>();
						TMDetails.put(TMType.BEAN, null);
						typesToTM.put(td, TMDetails);
						return;
					} else if (ap.getValue().toString().contains("CONTAINER")) {
						TAType ta = CallGraphUtils
								.tellWhichTransactionAttributeIsUsed(td
										.getAnnotations());

						TMDetails = new HashMap<TMType, TAType>();
						TMDetails.put(TMType.CONTAINER,
								ta == null ? TAType.REQUIRED : ta);

						typesToTM.put(td, TMDetails);
						return;
					}
				}
			}
		}
		if (typeToComponentKind.containsKey(td)) {
			ComponentKind kind = typeToComponentKind.get(td);
			if (kind == ComponentKind.EJB_SINGLETON
					|| kind == ComponentKind.EJB_STATEFUL
					|| kind == ComponentKind.EJB_STATELESS) {
				
				TMDetails = new HashMap<TMType, TAType>();
				
				
				TAType ta = CallGraphUtils
						.tellWhichTransactionAttributeIsUsed(td
								.getAnnotations());
				
				if(ta!= null){
					System.out.println("Нашли ТА у " + td.getName() + ", это " + ta +
							 "  а ведь ТМ-аннотации не было. Ву-ху!");
					TMDetails.put(TMType.CONTAINER, ta);
				} 
				else {
					System.out.println("Тип " + td.getName() + " это " + kind
							+ " без аннотаций. Проставили @Required");
					TMDetails.put(TMType.CONTAINER, TAType.REQUIRED);
				}
				typesToTM.put(td, TMDetails);
			}
		}
	}

	/**
	 * Получаем список вызываемых методов как отсортированную коллекцию из
	 * следующего метода. Т.е. по порядку вызова в ходе кода.
	 * 
	 * If we want to get all the called methods from this method declaration, we
	 * have to select all usages which are contained by this method declaration.
	 * It means all instances of AbstractMethodInvocation Yet, we have also to
	 * sort this collection by its order in call sequence
	 * 
	 * @param parent
	 * @return
	 */
	private final List<AbstractMethodInvocation> getCalledMethods(
			final AbstractMethodDeclaration parent) {
		List<AbstractMethodInvocation> invocations = getInvocations(parent);
		// sort invocations ...
		Collections.sort(invocations, new MethodInvocationComparator());
		return invocations;
	}

	/**
	 * Получаем все вызовы методов из текущего метода.
	 * 
	 * To retrieve all method invocations that are contained by this method
	 * declaration element.
	 * 
	 * @param parent
	 * @return
	 */
	private final List<AbstractMethodInvocation> getInvocations(
			final AbstractMethodDeclaration parent) {

		List<AbstractMethodInvocation> invocations = new ArrayList<AbstractMethodInvocation>();
		/*
		 * Итерируемся по всем методам, найденным для поекта и смотрим, не
		 * является ли НАШ метод ИНВОКЕРОМ (см. метод ниже) для текущего метода
		 * в итерации. если является - то добавляем текущий метод в список
		 * вызываемых для нашего.
		 */
		for (AbstractMethodInvocation invocation : this.allInvocations) {
			/*
			 * ЦИИКЛ!!! а что, если parent == getInvoker(invocation) ???
			 */
			if (parent == getInvoker(invocation)) {
				/*
				 * как найти имя типа для имени метода? чтобы сказать - АГА!
				 * Этот метод запрещен для
				 * блок.гетКласс.гетТипУправленияТранзакциями.
				 */
				invocations.add(invocation);
			}
		}
		return invocations;
	}

	/**
	 * Получаем тело метода, внутри которого находится данный вызов.
	 * 
	 * To retrieve the method declaration element which contains the source
	 * method invocation. Type of parameter element is EObject, because we have
	 * to navigate into statements hierarchy with many different kinds of
	 * elements
	 * 
	 * @param element
	 * @return
	 */
	private final AbstractMethodDeclaration getInvoker(final EObject element) {
		AbstractMethodDeclaration result = null;
		if (element != null) {
			if (element instanceof AbstractMethodDeclaration) {
				result = (AbstractMethodDeclaration) element;
			} else {
				result = getInvoker(element.eContainer());
			}
		}
		return result;
	}

	/*
	 * ХИТРОСПЛЕТЕННЫЙ КОМПАРАТОР MethodInvocationComparator сравниваются
	 * позиции методИнвокейшонов в объемлющем блоке (теле метода)
	 */
	protected static class MethodInvocationComparator implements
			Comparator<AbstractMethodInvocation> {

		public int compare(final AbstractMethodInvocation invocation1,
				final AbstractMethodInvocation invocation2) {

			int result = -1;
			/*
			 * Initially, I used location in file to sort invocations, but
			 * needed information is no longer retained in java model ...
			 * 
			 * Perhaps could I use index of parent element which is directly
			 * contained in block statement of the declaring method ?
			 */

			Block rootBlock1 = getRootBlock(invocation1);
			Block rootBlock2 = getRootBlock(invocation2);

			if ((rootBlock1 == null) || (rootBlock2 == null)) {
				result = 0;
			} else {
				int index1 = computeIndex(invocation1, rootBlock1);
				int index2 = computeIndex(invocation2, rootBlock2);
				/*
				 * specific case: both invocations have the same index and are
				 * contained in the same block.
				 * 
				 * So we have to retrieve first common parent block and use it
				 * to compute index ...
				 */
				if ((index1 == index2) && (rootBlock1 == rootBlock2)) {
					Block commonBlock = getFirstCommonParentBlock(invocation1,
							invocation2, rootBlock1);
					index1 = computeIndex(invocation1, commonBlock);
					index2 = computeIndex(invocation2, commonBlock);
				}

				result = Integer.valueOf(index1).compareTo(
						Integer.valueOf(index2));
			}
			return result;
		}

		private static final int INVALID_INDEX = -2;

		private final int computeIndex(final EObject element,
				final Block rootBlock) {
			int result = TXContextsGraphConverter.MethodInvocationComparator.INVALID_INDEX;
			if (element.eContainer() == rootBlock) {
				result = rootBlock.getStatements().indexOf(element);
			} else {
				result = computeIndex(element.eContainer(), rootBlock);
			}
			return result;
		}

		/*
		 * получаем ТЕЛО BLOCK вызываемого метода из элемента модели если
		 * элемент - контейнер, то разврачиваем рекурсивно
		 */
		private final Block getRootBlock(final EObject element) {
			Block result = null;
			if (element != null) {
				if (element instanceof AbstractMethodDeclaration) {
					result = ((AbstractMethodDeclaration) element).getBody();
				} else {
					result = getRootBlock(element.eContainer());
				}
			}
			return result;
		}

		/*
		 * To retrieve the first common parent block of two elements lets start
		 * from an example: block A contains another block B (with other
		 * statements) block B contains two blocks C and D which contains
		 * respectively element1 and element2
		 * 
		 * Here the algorithm: From element1, I get the nearest parent block
		 * Then I will iterate on all parent blocks of element2 and test
		 * identity If not found, I have to iterate on parent block of element1
		 * And test again on all parent blocks of element2 ...
		 */
		private final Block getFirstCommonParentBlock(
				final EObject sourceElement, final EObject element2,
				final Block stopper) {
			Block result = getParentBlock(sourceElement);
			if (result != stopper) {
				if (!isParentBlock(element2, result, stopper)) {
					result = getFirstCommonParentBlock(result, element2,
							stopper);
				} // else, result is the first common parent block
			}
			return result;
		}

		private final boolean isParentBlock(final EObject element,
				final Block target, final Block stopper) {
			boolean result = false;
			Block parent = getParentBlock(element);
			if (parent != stopper) {
				if (parent == target) {
					result = true;
				} else {
					result = isParentBlock(parent, target, stopper);
				}
			}
			return result;
		}

		private final Block getParentBlock(final EObject element) {
			Block result = null;
			if (element != null) {
				if (element.eContainer() instanceof Block) {
					result = (Block) element.eContainer();
				} else {
					result = getParentBlock(element.eContainer());
				}
			}
			return result;
		}
	}

	/**
	 * Получаем из некоторого EObject тип (TypeDeclaration) рекурсивно
	 * разворачиваем еобжект, если он контейнер
	 * 
	 * @param element
	 * @return
	 */
	private final TypeDeclaration getTypeDeclaration(final EObject element) {
		TypeDeclaration result = null;
		if (element != null) {
			if (element instanceof TypeDeclaration) {
				result = (TypeDeclaration) element;
			} else {
				result = getTypeDeclaration(element.eContainer());
			}
		}
		return result;
	}

	/**
	 * getAllSubTypes(final TypeDeclaration contextClass) Получаем все подтипы
	 * заданного типа как итерацию через все имеющиеся типы и добавление во
	 * множество результ найденного типа и его супертипов
	 * 
	 * @param contextClass
	 * @return
	 */
	private final Set<TypeDeclaration> getAllSubTypes(
			final TypeDeclaration contextClass) {

		Set<TypeDeclaration> result = new HashSet<TypeDeclaration>();

		if (contextClass != null) {

			// Итерируемся через найденные ТИПЫ приложения, this.allTypes
			for (TypeDeclaration currentClassDeclaration : this.allTypes) {
				if (isSuperTypeOf(contextClass, currentClassDeclaration)) {
					// если просматриваемый тип из this.allTypes является
					// супертипом для
					// contextClass, то добавляем текущий итерируемый ТИП и
					// его СУПЕРТИПЫ
					result.add(currentClassDeclaration);
					result.addAll(getAllSubTypes(currentClassDeclaration));

				}
			}
		}
		return result;
	}

	/**
	 * To compute a boolean which indicate if a type (self) is in parent
	 * hierarchy of an other type (typeDeclaration).
	 * 
	 * @param self
	 * @param typeDeclaration
	 * @return true if self is a super type of typeDeclaration
	 */

	/*
	 * Определяем, является ли передаваемый тип предком другого типа -
	 * typeDeclaration КЛАССНОЕ DISPATCH PROBLEM
	 */
	private final boolean isSuperTypeOf(final TypeDeclaration self,
			final TypeDeclaration typeDeclaration) {

		// если в иерархии наследования второго типа есть селф - то и сказочке
		// конец.
		if (typeDeclaration.getSuperInterfaces().contains(self)) {
			return true;
		}

		// если селф находится на какой-либо боковой ветке наследования одного
		// из суперИнтерфейсов
		// т.е. не прямо на иерархии.
		for (TypeAccess superTypeAccess : typeDeclaration.getSuperInterfaces()) {
			if (superTypeAccess.getType() instanceof TypeDeclaration) {
				TypeDeclaration superType = (TypeDeclaration) superTypeAccess
						.getType();
				if (superType == self || isSuperTypeOf(self, superType)) {
					return true;
				}
			}
		}

		// если второй тип является не абы чем, а КЛАССОМ:
		if (typeDeclaration instanceof ClassDeclaration) {
			ClassDeclaration classDeclaration = (ClassDeclaration) typeDeclaration;

			// если селф является прямым предком (ОТЦОМ) второго типа
			if (classDeclaration.getSuperClass() != null
					&& classDeclaration.getSuperClass().getType() == self) {
				return true;
			}

			// если не является прямым отцом, а, возможно, находится где-то
			// сбоку
			if (classDeclaration.getSuperClass() != null
					&& classDeclaration.getSuperClass().getType() instanceof TypeDeclaration) {

				TypeDeclaration superType = (TypeDeclaration) classDeclaration
						.getSuperClass().getType();

				if (isSuperTypeOf(self, superType)) {
					return true;
				}
			}

		}

		return false;
	}

	/**
	 * По месту в коде, где вызывается метод, находим переменную, на которой
	 * метод вызывается
	 * 
	 * @param abstractMethodInvocation
	 * @return
	 */
	private static final VariableDeclaration getVariableDeclaration(
			final AbstractMethodInvocation abstractMethodInvocation) {

		VariableDeclaration result = null;

		if (abstractMethodInvocation instanceof MethodInvocation) {

			MethodInvocation methodInvocation = (MethodInvocation) abstractMethodInvocation;

			// у MethodInvocation есть выражение, в котором весь этот Invocation
			// и происходит
			if ((methodInvocation.getExpression() != null)
					&& (methodInvocation.getExpression() instanceof SingleVariableAccess)) {

				// если это выражение предст собой обращение к переменной, то
				// хватаем эту
				// переменную и определяем ее Declaration
				SingleVariableAccess singleVariableAccess = (SingleVariableAccess) methodInvocation
						.getExpression();

				result = singleVariableAccess.getVariable();

			}
		}
		return result;
	}

	/**
	 * Общий метод для получения потенциальных типов Вызывается private final
	 * Set<TypeDeclaration> getBasicFilteredPotentialTypes( final
	 * VariableDeclaration source, final List<VariableDeclaration> parents)
	 * 
	 * @param variableDeclaration
	 * @return
	 */
	private final Set<TypeDeclaration> getFilteredPotentialTypes(
			final VariableDeclaration variableDeclaration) {

		// похоже, эти parents и не используются нигде
		List<VariableDeclaration> parents = new ArrayList<VariableDeclaration>();

		Set<TypeDeclaration> result = getBasicFilteredPotentialTypes(
				variableDeclaration, parents);

		return result;
	}

	/**
	 * Ищем потенциальные типы по выражениям инициализации переменных типами X x
	 * = new X();
	 */
	private final Set<TypeDeclaration> getBasicFilteredPotentialTypes(
			final VariableDeclaration source,
			final List<VariableDeclaration> parents) {

		Set<TypeDeclaration> result = new HashSet<TypeDeclaration>();

		// Добавляем в лист только те VariableDeclaration, которых там еще не
		// было
		if (!parents.contains(source)) {
			parents.add(source);

			// Получаем инициализатор объявления переменной
			Expression initializer = source.getInitializer();

			// Если переменная инициализируется созданием экза класса, то
			// ну и ок, добавляем TypeDeclaration для этого класса в result.
			if ((initializer != null)
					&& (initializer instanceof ClassInstanceCreation)) {

				ClassInstanceCreation classInstanceCreation = (ClassInstanceCreation) initializer;

				if (classInstanceCreation.getType().getType() instanceof TypeDeclaration) {
					result.add((TypeDeclaration) classInstanceCreation
							.getType().getType());
				}
			}
			// в любом случае, добавляем в резалт все то, что найдет
			// filterAssignement(source, parents)
			// ВОУ, а не избыточность ли тут?
			// чоэта мы 2 раза добавляем в резалт?
			result.addAll(filterAssignement(source, parents));
		}

		return result;
	}

	// Выясяем, какие типы инициализировались в присваиваниях
	private final Set<TypeDeclaration> filterAssignement(
			final VariableDeclaration source,
			final List<VariableDeclaration> parents) {

		Set<TypeDeclaration> result = new HashSet<TypeDeclaration>();

		for (SingleVariableAccess access : source.getUsageInVariableAccess()) {

			EObject container = access.eContainer();

			// нашли все использования переменной с типом ПРИСВАИВАНИЕ
			if (container instanceof Assignment) {

				Assignment assignment = (Assignment) container;
				Expression expression = assignment.getRightHandSide();

				// Если правая часть присваивания - класс открытым текстом,
				// то берем тип этого класса и запихиваем в result
				if (expression instanceof ClassInstanceCreation) {
					ClassInstanceCreation classInstanceCreation = (ClassInstanceCreation) expression;
					if (classInstanceCreation.getType().getType() instanceof TypeDeclaration) {
						result.add((TypeDeclaration) classInstanceCreation
								.getType().getType());
					}
				}

				// если какое-то другое выражение, то вызываем
				// getBasicFilteredPotentialTypes(variable, parents)
				// в надежде рано или поздно дойти до типа.
				else if (expression instanceof SingleVariableAccess) {
					/*
					 * potential infinite recursion
					 * 
					 * Object tmp; Object src = tmp; ... tmp = src;
					 */
					SingleVariableAccess singleVariableAccess = (SingleVariableAccess) expression;
					result.addAll(getBasicFilteredPotentialTypes(
							singleVariableAccess.getVariable(), parents));
				}
			}
		}
		return result;
	}

	private void getEJBs(IFile ejbjarResource) {
		EjbJarExplorer eje = new EjbJarExplorer();
		if (ejbjarResource != null)
			eje.exploreEjbJar(ejbjarResource.getLocation().toString());
		
		ejbNamesToClasses = eje.getEjbNamesToClasses();
		classNamesToKinds = eje.getClassNamesToKinds();
		classNamesToTMs = eje.getClassNamesToTMs();
		methodsToTAs = eje.getMethodsToTAs();
	}

	public J2EECallGraphMetadata getJ2EECallGraphMetadata() {
		J2EECallGraphMetadata metadata = new J2EECallGraphMetadata();

		metadata.setJavaOperationToEntryPoint(javaOperationToEntryPoint);
		metadata.setTypesToTM(typesToTM);
		metadata.setTypeToComponentKind(typeToComponentKind);
		metadata.setJavaOperationToTA(javaOperationToTA);

		return metadata;
	}
}
