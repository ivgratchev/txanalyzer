/**
 * Copyright (c) 2010, 2011 Mia-Software.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Gabriel Barbier (Mia-Software) - initial API and implementation
 *    Fabien Giquel (Mia-Software) - Bug 339720 : MoDisco Discoverers (infra + techno) API clean
 *    Nicolas Bros (Mia-Software) - Bug 335003 - [Discoverer] : Existing Discoverers Refactoring based on new framework
 *******************************************************************************/

package ru.vlsu.ispi.txcontexts.discoverer;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.LinkedList;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gmt.modisco.infra.common.core.internal.utils.ModelUtils;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.modisco.infra.discovery.core.AbstractModelDiscoverer;
import org.eclipse.modisco.infra.discovery.core.IDiscoveryManager;
import org.eclipse.modisco.infra.discovery.core.exception.DiscoveryException;
import org.eclipse.modisco.java.discoverer.JavaModelUtils;
import org.eclipse.modisco.jee.ejbjar.discoverer.EjbJarDiscoverer2;
import org.eclipse.modisco.jee.webapp.discoverer.WebXmlDiscoverer2;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.CallNode;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.CallsModel;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.MethodCall;

import ru.vlsu.ispi.txcontexts.TXContextsConstants;
import ru.vlsu.ispi.txcontexts.analyzer.Analyzer;
import ru.vlsu.ispi.txcontexts.analyzer.TXContext;
import ru.vlsu.ispi.txcontexts.discoverer.converter.J2EECallGraphMetadata;
import ru.vlsu.ispi.txcontexts.discoverer.converter.TXContextsGraphConverter;

public class DiscoverTXContextsModelFromJavaModel extends
		AbstractModelDiscoverer<IFile> {

	public boolean isApplicableTo(final IFile source) {
		return JavaModelUtils.isJavaModelFile(source);
	}

	@Override
	protected void basicDiscoverElement(final IFile file,
			final IProgressMonitor monitor) throws DiscoveryException {
		IProject project = file.getProject();
		System.out.println("ИМЯ ПРОЕКТА: " + project.getName());
		IJavaProject javaProject = null;
		try {
			if (project.hasNature(JavaCore.NATURE_ID)) {
				javaProject = JavaCore.create(project);
			}
		} catch (CoreException e) {
			throw new DiscoveryException(e);
		}
		if (javaProject != null) {
			Resource javaResource = loadJavaModel(file);
			IFile ejbjarResource = findEjbJar(project);
			//Resource ejbjarResource = loadEJBJarModel();
			String filepath = project.getFullPath().append(project.getName())
					+ TXContextsConstants.MODEL_EXTENSION;
			System.out.println("ФАЙЛПАС " + filepath);
			setDefaultTargetURI(URI.createPlatformResourceURI(filepath, true));

			TXContextsGraphConverter callsConverter = new TXContextsGraphConverter();
			
			CallsModel callsModel = callsConverter.convertJavaResourceToTXContextsModel(javaResource,
					ejbjarResource, project.getName());

			
			J2EECallGraphMetadata metadata = callsConverter.getJ2EECallGraphMetadata();
			Analyzer a = new Analyzer(metadata);
			TXContext.maxTXIdSoFar = 0;
			
			DOTGraphExtractor dge = new DOTGraphExtractor(a);
			String str = dge.computeDOTGraph(callsModel);
			
			IFile dotFile = project.getFile(project.getName() + TXContextsConstants.DOT_EXTENSION);
			try {
				InputStream source = new ByteArrayInputStream(str.getBytes("UTF-8"));
				if (!dotFile.exists())
					dotFile.create(source, IResource.DERIVED, monitor);
				else
					dotFile.setContents(source, false, false, monitor);
			} catch (Exception e) {
				throw new DiscoveryException(e);
			}
			
			//System.out.println(str);
			
			/*
			printMethodTrace(callsModel.getRootNodes(), "/home/andrey/Рабочий стол/1Ctask/TX.txt", "RootNodes");
			printMethodTrace(callsModel.getCallNodes(), "/home/andrey/Рабочий стол/1Ctask/TX2.txt", "CallNodes");
*/
			createTargetModel();

			getTargetModel().getContents().add(callsModel);
		}
	}
	
	private IFile findEjbJar(IProject project) {
		final LinkedList<IFile> resultList = new LinkedList<IFile>();
		try {
			project.accept(new IResourceVisitor() {
				
				public boolean visit(IResource resource) throws CoreException {
					if (resource.getType() != IResource.FILE)
						return true;
					if (resource.getName().equals("ejb-jar.xml"))
						resultList.add((IFile) resource);
					return false;
				}
			});
		} catch (CoreException e) {
			e.printStackTrace();
		}
		return resultList.size() > 0 ? resultList.getFirst() : null;
	}

	private void printChain(CallNode cn, PrintWriter pw){
		
		EList<MethodCall> mcalls = cn.getMethodCalls();

		if (mcalls.size() == 0) {
			pw.println('\t' + " нет");
		} else {
			for (MethodCall ms : mcalls) {
				pw.println('\t' + ms.getCallee().getName());
				printChain(ms.getCallee(), pw);
			}
		}
	}
	/*
	private void advancedPrint(CallNode cn, PrintWriter pw){
		if(cn != null){
			pw.print(cn.getName() + " -> ");
			if(cn.getMethodCalls().size()>0){
				cn = cn.getMethodCalls().get(0).getCallee();
				advancedPrint(cn, pw);
			}
		}
	} */
	
	
	private void printMethodTrace(EList<CallNode> calls, String filePath, String label){
		//int i = 1;
		
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(filePath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		pw.println(label+"\n");
		//EList<CallNode> calls = callsModel.getCallNodes();
		for (CallNode cn : calls) {
			pw.println("НОВАЯ ЦЕПОЧКА");
			pw.println("  " + cn.getName());
			// pw.println(i++ + cn.getJavaMethod().getName());// - почти то
			// же. и то и то - метод.

			pw.println(" \nМетод коллы");
			EList<MethodCall> mcalls = cn.getMethodCalls();

			if (mcalls.size() == 0) {
				pw.println('\t' + " нет");
			} else {
				for (MethodCall ms : mcalls) {
					//pw.println('\t' +  ms.getCallee().getName());
					printChain(ms.getCallee(), pw);
				}
			}
			/*
			pw.println(" \nСубМетоды");
			EList<CallNode> cns = cn.getSubMethods();

			if (cns.size() == 0) {
				pw.println('\t' + " нет");
			} else {
				for (CallNode c : cns) {
					//pw.println('\t'  + c.getJavaMethod().getName());
					printChain(c, pw);
				}
			}*/
			pw.println();
		}
		pw.flush();
	}
	
	private Resource loadEJBJarModel() {
		EjbJarDiscoverer2 discoverer = (EjbJarDiscoverer2) IDiscoveryManager.INSTANCE
				.createDiscovererImpl(EjbJarDiscoverer2.ID);
		File ejbjarFile = new File(
				"/home/andrey/windows_drives/D/__eclipse_clean/workspace/CarnetContactsEJB31/WebContent/WEB-INF/ejb-jar.xml");
		try {
			discoverer.discoverElement(ejbjarFile, new NullProgressMonitor());
		} catch (DiscoveryException e) {
			e.printStackTrace();
		}
		return discoverer.getTargetModel();
	}

	private Resource loadWebAppModel() {
		WebXmlDiscoverer2 discoverer = new WebXmlDiscoverer2();
		File webxmlFile = new File(
				"C:\\dev\\runtime-EclipseApplication\\CarnetContactsEJB31\\WebContent\\WEB-INF\\web.xml");
		try {
			discoverer.discoverElement(webxmlFile, new NullProgressMonitor());
		} catch (DiscoveryException e) {
			e.printStackTrace();
		}
		return discoverer.getTargetModel();
	}

	private static Resource loadJavaModel(final IFile file)
			throws DiscoveryException {
		Resource result = null;
		try {
			result = ModelUtils.loadModel(file.getLocation().toFile());
		} catch (IOException e) {
			throw new DiscoveryException(e);
		}
		return result;
	}

}
