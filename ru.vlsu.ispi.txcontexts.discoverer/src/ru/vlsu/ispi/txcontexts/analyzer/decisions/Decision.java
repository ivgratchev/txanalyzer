package ru.vlsu.ispi.txcontexts.analyzer.decisions;

import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.CallNode;

public abstract class Decision {
	public Decision(){}
	
	public Decision(String d){
		description = d;
	}
	
	public Decision(CallNode m, String d){
		containingNode = m;
		description = d;
	}
	
	protected CallNode containingNode;
	protected String description;
	
	public CallNode getConatainingNode() {
		return containingNode;
	}
	public void setConatainingNode(CallNode conatainingNode) {
		this.containingNode = conatainingNode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
