package ru.vlsu.ispi.txcontexts.analyzer.decisions;

import org.eclipse.gmt.modisco.java.AbstractMethodDeclaration;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.CallNode;

public class Error extends Decision{
	public Error(){}
	
	public Error(String d){
		super(d);
	}
	
	public Error(CallNode m, String d){
		super(m,d);
	}
	
	public static final String NOT_REACHABLE_METHOD_STRING = "Недостижимый метод";
	public static final String EP_ABNORMAL_STOP_STRING = "Аварийное завершение";
	
	public static Error getNewNotReacheableMethodError(CallNode m){
		Error e = new Error(NOT_REACHABLE_METHOD_STRING);
		e.setConatainingNode(m);
		return e;
	}
	
	public static Error getNewEPError(CallNode m){
		Error e = new Error(EP_ABNORMAL_STOP_STRING);
		e.setConatainingNode(m);
		return e;
	}
	
	public static Error getNewGenericError(){
		Error e = new Error("");
		return e;
	}
}
