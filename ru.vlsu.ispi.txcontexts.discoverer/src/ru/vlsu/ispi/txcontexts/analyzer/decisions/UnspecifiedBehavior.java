package ru.vlsu.ispi.txcontexts.analyzer.decisions;

import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.CallNode;

public class UnspecifiedBehavior extends Decision{
	public UnspecifiedBehavior(){}
	
	public UnspecifiedBehavior(CallNode m, String d){
		super(m,d);
	}
	
}
