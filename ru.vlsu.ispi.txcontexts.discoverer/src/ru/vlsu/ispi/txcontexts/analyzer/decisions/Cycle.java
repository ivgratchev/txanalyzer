package ru.vlsu.ispi.txcontexts.analyzer.decisions;

public class Cycle extends Decision{
	
	public Cycle(){
		description = "Циклический\\nвызов";
	}
	
	private int firstCallIndex = -1;
	
	public int getFirstCallIndex(){
		System.out.println("Цикл возвращает индекс первого вхождения: " + firstCallIndex);
		return firstCallIndex;
	}
	
	public void setFirstCallIndex(int i){
		System.out.println("Цикл устанавливает индекс первого вхождения: " + i);
		firstCallIndex = i;
	}
	
}
