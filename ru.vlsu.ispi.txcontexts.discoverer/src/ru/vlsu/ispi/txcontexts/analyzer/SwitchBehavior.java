package ru.vlsu.ispi.txcontexts.analyzer;

public enum SwitchBehavior {
	PROPAGATE, SUSPEND, START_NEW, SUSPEND_AND_START_NEW, NONE;
}
