package ru.vlsu.ispi.txcontexts.analyzer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gmt.modisco.java.AbstractMethodDeclaration;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.CallNode;

import ru.vlsu.ispi.txcontexts.discoverer.converter.TMType;

public class TXContext {
	
	public TXContext(){
		txId = NO_TRANSACTION_ID;
		tmType = TMType.CONTAINER;
	};
	
	public TXContext(int txId){
		this.txId = txId;
		tmType = TMType.CONTAINER;
	};
	
	public TXContext(int txId, TMType tm){
		this.txId = txId;
		tmType = tm;
	};
	
	private TXContext(int txId, TMType tm, boolean unspecified){
		this(txId, tm);
		this.unspecified = unspecified;
	}
	
	//имеет смысл только для Bean-managed транзакций.
	//для CM - не просматриваем  это поле.
	private boolean unclosed = false;
	private boolean unspecified = false;

	public static final int NO_TRANSACTION_ID = 0;
	public static final TXContext NO_TRANSACTION_CONTEXT = new TXContext(NO_TRANSACTION_ID, null);
	public static final TXContext UNSPECIFIED_TRANSACTION_CONTEXT = new TXContext(NO_TRANSACTION_ID, TMType.CONTAINER, true);
	public static int maxTXIdSoFar = 0;
	
	private SwitchBehavior behavior;

	public boolean isUnspecified() {
		return unspecified;
	}

	public void setUnspecified(boolean unspecified) {
		this.unspecified = unspecified;
	}

	public static TXContext getNewContainerTXContext(){
		TXContext tc = new TXContext(++maxTXIdSoFar);
		tc.setUnclosed(false);
		tc.setTmType(TMType.CONTAINER);
		tc.setBehavior(SwitchBehavior.START_NEW);
		return tc;
	}
	
	public static TXContext getNewBeanTXContext(){
		TXContext tc = new TXContext(++maxTXIdSoFar);
		tc.setUnclosed(true);
		tc.setTmType(TMType.BEAN);
		return tc;
	}

	private List<Integer> nodeIndexes = new ArrayList<Integer>();
	private List<CallNode> methodsInContext = new ArrayList<CallNode>();
	
	private int txId;
	
	private TMType tmType = TMType.CONTAINER;
	
	public TMType getTmType() {
		return tmType;
	}

	public void setTmType(TMType tmType) {
		this.tmType = tmType;
	}

	public List<CallNode> getCallNodesInContext() {
		return methodsInContext;
	}

	public void setMethodsInContext(List<CallNode> methodsInContext) {
		this.methodsInContext = methodsInContext;
	}

	public TXContext startNewTXContext(int startNodeIndex){
		maxTXIdSoFar++;
		return new TXContext();
	}
	
	public List<Integer> getNodeIndexes() {
		return nodeIndexes;
	}
	public void setNodeIndexes(List<Integer> nodeIndexes) {
		this.nodeIndexes = nodeIndexes;
	}
	public int getTxId() {
		return txId;
	}
	public void setTxId(int txId) {
		this.txId = txId;
	}
	
	public boolean isUnclosed() {
		return unclosed;
	}

	public void setUnclosed(boolean unclosed) {
		this.unclosed = unclosed;
	}
	
	public SwitchBehavior getBehavior() {
		return behavior;
	}

	public void setBehavior(SwitchBehavior behavior) {
		this.behavior = behavior;
	}
	
	public boolean equals(Object o){
		if(o instanceof TXContext){
			return  ((TXContext)o).getTxId() == txId;
		}
		return false;
	}
	
}
