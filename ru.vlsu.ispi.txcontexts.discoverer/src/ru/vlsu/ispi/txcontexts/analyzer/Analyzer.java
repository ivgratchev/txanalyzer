package ru.vlsu.ispi.txcontexts.analyzer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.gmt.modisco.java.AbstractMethodDeclaration;
import org.eclipse.gmt.modisco.java.AbstractTypeDeclaration;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.CallNode;

import ru.vlsu.ispi.txcontexts.analyzer.decisions.Cycle;
import ru.vlsu.ispi.txcontexts.analyzer.decisions.Decision;
import ru.vlsu.ispi.txcontexts.analyzer.decisions.Error;
import ru.vlsu.ispi.txcontexts.analyzer.decisions.Redundancy;
import ru.vlsu.ispi.txcontexts.analyzer.decisions.UnspecifiedBehavior;
import ru.vlsu.ispi.txcontexts.discoverer.DOTGraphExtractor;
import ru.vlsu.ispi.txcontexts.discoverer.converter.CallGraphUtils;
import ru.vlsu.ispi.txcontexts.discoverer.converter.ComponentKind;
import ru.vlsu.ispi.txcontexts.discoverer.converter.J2EECallGraphMetadata;
import ru.vlsu.ispi.txcontexts.discoverer.converter.TAType;
import ru.vlsu.ispi.txcontexts.discoverer.converter.TMType;

public class Analyzer {

	public Analyzer(J2EECallGraphMetadata metadata) {
		this.metadata = metadata; 
		typesToTM = metadata.getTypesToTM();
		//javaOperationToEntryPoint = metadata.getJavaOperationToEntryPoint();
		typeToComponentKind = metadata.getTypeToComponentKind();
		javaOperationToTA = metadata.getJavaOperationToTA();
		
		preparejavaOperationToTA();
		// UTControlMethodNames = Arrays.asList();
	}
	
	private void preparejavaOperationToTA(){
		Set<Entry<AbstractMethodDeclaration, TAType>> entries = javaOperationToTA.entrySet();
		
		Iterator<Entry<AbstractMethodDeclaration, TAType>> iterator = entries.iterator();
		
		while(iterator.hasNext()){
			Entry<AbstractMethodDeclaration, TAType> entry = iterator.next();
			AbstractMethodDeclaration method = entry.getKey();
			
			if(CallGraphUtils.methodIsLifeCycleCallbackWithUnspecifiedBehavior(method)){
				iterator.remove();
				//javaOperationToTA.remove(method);
				System.out.println("Удалили метод");
			}
		}
	}
	// отображение СТЕЙТФУЛ компонента с бин-менеджед транзакциями на тот контекст, который в нем есть.
	private HashMap<AbstractTypeDeclaration, TXContext> BMBeansToTXContexts = new HashMap<AbstractTypeDeclaration, TXContext>();
	/*
	 * 
	 * Добавить какой-нито Map<Type, TXContext> transactionInCurrentBMComponenent
	 * проверять, когда заходим в новый метод:
	 * transactionInCurrentBMComponenent.get(method.getAbstractTypeDeclaration()) != null?
	 * currentContext = ^^^^^^
	 * methodContext = ^^^^^
	 * 
	 */

	private List< Map<CallNode, Map<Integer, SwitchBehavior>> > callHistory = new ArrayList<Map<CallNode, Map<Integer, SwitchBehavior>> >();
	
	private Map<AbstractTypeDeclaration, Map<TMType, TAType>> typesToTM;
	//private Map<AbstractMethodDeclaration, EntryPoint> javaOperationToEntryPoint;
	private Map<AbstractTypeDeclaration, ComponentKind> typeToComponentKind;
	private Map<AbstractMethodDeclaration, TAType> javaOperationToTA = new HashMap<AbstractMethodDeclaration, TAType>();

	private final String userTransactionPackage = "javax.transaction.UserTransaction";
	private final String EJBContextPackage = "javax.ejb.EJBContext";
	private final String SQLConnectionPackage = "java.sql.Connection";
	private final String entityManagerPackage = "javax.persistence.EntityManager";
	private final String queryPackage = "javax.persistence.Query";
	private final String sqlPackage = "java.sql";
	
	private final String[] UTControlMethodNames = {
			userTransactionPackage + " :: begin",
			userTransactionPackage + " :: commit",
			userTransactionPackage + " :: rollback" 
	};

	private final String[] BMProhibitedMethods = {
			EJBContextPackage + " :: getRollbackOnly",
			EJBContextPackage + " :: setRollbackOnly"
	};
	
	private final String[] CMProhibitedMethods = {
			SQLConnectionPackage + " :: commit",
			SQLConnectionPackage + " :: setAutoCommit",
			SQLConnectionPackage + " :: rollback",
			EJBContextPackage + " :: getUserTransaction"
	};
	
	private final String[] meaningfulDBOperations = {
			sqlPackage + ".Statement :: execute",
			sqlPackage + ".Statement :: executeQuery",
			sqlPackage + ".Statement :: executeUpdate",
			sqlPackage + ".Statement :: executeBatch",
			
			sqlPackage + ".PreparedStatement :: execute",
			sqlPackage + ".PreparedStatement :: executeQuery",
			sqlPackage + ".PreparedStatement :: executeUpdate",
			sqlPackage + ".PreparedStatement :: executeBatch",
			
			sqlPackage + ".CallableStatement :: execute",
			sqlPackage + ".CallableStatement :: executeQuery",
			sqlPackage + ".CallableStatement :: executeUpdate",
			sqlPackage + ".CallableStatement :: executeBatch",
			
			entityManagerPackage + " :: find",
			entityManagerPackage + " :: flush",
			entityManagerPackage + " :: joinTransaction",
			entityManagerPackage + " :: lock",
			entityManagerPackage + " :: merge",
			entityManagerPackage + " :: persist",
			entityManagerPackage + " :: refresh",
			entityManagerPackage + " :: remove",
			
			queryPackage + " :: executeUpdate",
			queryPackage + " :: getSingleResult",
			queryPackage + " :: getResultList",
	};
	
	private final String afterCompletionAnnotation = "AfterCompletion";
	
	private final String synchronizationInterface = "javax.transaction.Synchronization";
	private final String afterCompletionMethod =  "afterCompletion(int)";
	private final String beforeCompletionMethod =  "beforeCompletion()";	
	
	
	//private final TAType[] UnspecifiedTAAnnotations = {TAType.SUPPORTS, TAType.NEVER, TAType.NOT_SUPPORTED};
	
	
	private static J2EECallGraphMetadata metadata;
	
	private String getNameOnly(String nameWithParams) {
		
		return nameWithParams.substring(0, nameWithParams.indexOf("("));
	}
	
	private String isCurrentMethodUTControllable(String methodName) {
		//System.out.print("Полное имя метода " + methodName + " --- ");
		String nameOnly = getNameOnly(methodName);
		//System.out.println("Имя метода без скобок " + nameOnly);
		
		for (String s : UTControlMethodNames) {
			//System.out.println(s + " vs name Only " + nameOnly);
			if (s.equals(nameOnly)) {
				System.out.println("Найден упрметод " + s);
				return s;
			}
		}
		return null;
	}
	
	public boolean checkIfCallerHasUnspecifiedBehavior(CallNode callerNode){
			AbstractTypeDeclaration type = callerNode.getJavaMethod().getAbstractTypeDeclaration();
			ComponentKind kind = typeToComponentKind.get(type);
			
			if(CallGraphUtils.methodIsLifeCycleCallbackWithUnspecifiedBehavior(callerNode)){
				//System.out.println(callerNode.getName() + " - это ЖЦ интерсепт с неопр пов");
				return true;
			}
			
			if(kind == ComponentKind.EJB_SINGLETON
			   || kind == ComponentKind.EJB_STATEFUL
			   || kind == ComponentKind.EJB_STATELESS){
				
				//System.out.println("after Тип = это " + kind);
				
				if(CallGraphUtils
						.methodDeclarationHasAnnotationSpecified(callerNode.getJavaMethod(), afterCompletionAnnotation)){
					//System.out.println("У метода " + callerNode.getName() + " нашли аннотацию " + afterCompletionAnnotation + " а это неопр конт");
					return true;
				}
				
				if(callerNode.getName().contains(afterCompletionMethod) && CallGraphUtils.typeIsSubtypeOf(type, synchronizationInterface)){
					//System.out.println("Сработало самое сложное\nМетод " + callerNode.getName() + " находится в типе, расш Synchroniztaion" +  "а это неопр конт");
					return true;
				} else if(callerNode.getName().contains(beforeCompletionMethod) && CallGraphUtils.typeIsSubtypeOf(type, synchronizationInterface)){
					//System.out.println("Сработало самое сложное\nМетод " + callerNode.getName() + " находится в типе, расш Synchroniztaion" +  "а это неопр конт");
					return true;
				}
				
			}
		return false;
	}
	
	private boolean isCurrentMethodBMProhibited(String methodName){
		for (String s : BMProhibitedMethods) {
			//System.out.println(s + " vs name Only " + nameOnly);
			if (s.equals(methodName)) {
				System.out.println("Найден запр в BM метод " + s);
				return true;
			}
		}
		return false;
	}
	
	private boolean isCurrentMethodCMProhibited(String methodName){
		if(methodName.startsWith(userTransactionPackage)){
			System.out.println("В CM нашли вызов UT-метода! Эррор!");
			return true;
		}
		else{
			for(String s: CMProhibitedMethods){
				if(methodName.equals(s)){
					System.out.println("Нашли СМ prohobited method " + s);
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean isMethodProhibitedInCurrentTMType(TMType tm, String methodName){
		switch(tm){
			case CONTAINER:
				return isCurrentMethodCMProhibited(methodName);
			case BEAN:
				return isCurrentMethodBMProhibited(methodName);
			default:
				return false;
		}
	}

	public ContextSwitch inspectTC(CallNode callerNode, CallNode calleeNode, TXContext callerContext) {
		AbstractMethodDeclaration callerMethod = null;
		
		ContextSwitch contextSwitch = new ContextSwitch();
		contextSwitch.setContext(callerContext);
		
		boolean cycleDetected = false;
		SwitchBehavior behavior = null;
		
		if (callerNode != null) {
			callerMethod = callerNode.getJavaMethod();
		}
		
		AbstractMethodDeclaration calleeMethod = calleeNode.getJavaMethod();
		
		Map<TMType, TAType> calleeMap = metadata.getTypesToTM().get(
				calleeMethod.getAbstractTypeDeclaration());
		TMType calleeTm = calleeMap != null ? calleeMap.keySet().iterator().next() : null;
		
		Map<TMType, TAType> callerMap = null;
		TMType callerTM = null;
		
		System.out.println("КОЛЛЕР МЕТОД!!! " + callerMethod);
		
		if(callerMethod != null){
			callerMap = metadata.getTypesToTM().get(callerMethod.getAbstractTypeDeclaration());
			System.out.println("КОЛЛЕР МЭП!!! " + callerMap);
			if(callerMap != null){
				callerTM = callerMap.keySet().iterator().next();
			}
		}
		
		AbstractTypeDeclaration callerTypeWithBeanTM = null;
		
		if (callerTM != null) {
			if (callerTM == TMType.BEAN) {
				//System.out.println("Контекст вызыватора " + callerTM);
				callerTypeWithBeanTM = null;
				ComponentKind kind = null;
				
				if (callerMethod != null){
					callerTypeWithBeanTM = callerMethod.getAbstractTypeDeclaration();
					//TMType tm = typesToTM.get(beanType).keySet().iterator().next();
					kind = typeToComponentKind.get(callerTypeWithBeanTM);
				}
				
				TXContext beanContext = null;
				
				if(callerTypeWithBeanTM != null && kind != null){
					beanContext = BMBeansToTXContexts.get(callerTypeWithBeanTM);
				}
				
				if(beanContext != null){
					System.out.println("C экземпляром была ассоциирована bean-тр, " + beanContext.getTxId());
					System.out.println("Взяли ее и продолжили");
					callerContext = beanContext;
				}
				
				String controlMethodName = null;
				if ((controlMethodName = isCurrentMethodUTControllable(
						calleeNode.getName() /* было коллер*/ )) != null) {
					contextSwitch = performBeanContextSwitch(calleeNode, callerContext);

					// Тут мы смотрим, что вернул контекст свич. если переключились в пустую транзакцию,
					// значит, сработал коммит или ролбэк и текущую-ассоциированную надо удалить.
					if (contextSwitch.getContext().getTxId() == TXContext.NO_TRANSACTION_ID) {
						System.out.println("Удалили предыдущий конекст из BEAN " + callerContext.getTxId());
						BMBeansToTXContexts.remove(callerTypeWithBeanTM);
					} else {
						System.out.println("Запустили контекст в BEAN " + contextSwitch.getContext().getTxId());
						BMBeansToTXContexts.put(callerTypeWithBeanTM, contextSwitch.getContext());
					}
					return contextSwitch;
				} else if (calleeTm != TMType.CONTAINER) {
					if (callerContext.getTxId() > 0) {
						behavior = SwitchBehavior.PROPAGATE;
					} else {
						behavior = SwitchBehavior.NONE;
					}
					//просто засовываем метод в тр, иниц ЮТ.бегин
					contextSwitch.setContext(callerContext);
					cycleDetected = !putCalleeNodeInCallHistory(calleeNode, DOTGraphExtractor.index, behavior);
					if(cycleDetected){
						Cycle c = new Cycle();
						c.setFirstCallIndex(getFirstCallIndex(calleeNode, behavior));
						contextSwitch.setDecision(c);
					}
					return contextSwitch;
				}
			}
		}
		
		if (calleeMap != null) {
			if (calleeTm == TMType.CONTAINER) {
				AbstractTypeDeclaration currentType = calleeMethod.getAbstractTypeDeclaration();
				if (callerNode == null) {
					System.out.println("+++ Вызывается метод " + calleeNode.getName() + " как точка входа ");
					
					contextSwitch = performContainerContextSwitch(callerContext, calleeNode);

					return contextSwitch;
				} else if (currentType.equals(callerMethod.getAbstractTypeDeclaration())) {
					// TODO: проверить, что будет в случае вызова метода того же типа, но через представление (интерфейс)
					System.out.println("+++ Вызываются методы из одного типа " + calleeNode.getName() + "#" + calleeMethod.getName() + " из "  + callerNode.getName());
					// Забыл вставить сюда.
					if(callerContext.getTxId() > 0){
						behavior = SwitchBehavior.PROPAGATE;
					} else {
						behavior = SwitchBehavior.NONE;
					}
					cycleDetected = !putCalleeNodeInCallHistory(calleeNode, DOTGraphExtractor.index, behavior);
					if(cycleDetected){
						Cycle c = new Cycle();
						c.setFirstCallIndex(getFirstCallIndex(calleeNode, behavior));
						contextSwitch.setDecision(c);
					}
					return contextSwitch;
				}else if (!currentType.equals(callerMethod.getAbstractTypeDeclaration())) {
					System.out.println("+++ Вызывается метод " + calleeNode.getName() + " из "  + callerNode.getName());
					
					contextSwitch = performContainerContextSwitch(callerContext, calleeNode);
					return contextSwitch;
				}
				
			} else if (calleeTm == TMType.BEAN){
				System.out.println("Вызывается проектный метод с БИН-управлением " + calleeNode.getName());
				if(callerTM != null){
					if(callerTM==TMType.CONTAINER){
						if(callerContext.getTxId() > 0){
							System.out.println(" -- приостановили контекст вызыватора.");
							contextSwitch.setContext(TXContext.NO_TRANSACTION_CONTEXT);
							behavior = SwitchBehavior.SUSPEND;
						} else {
							//а никакого контекста и не было
							behavior = SwitchBehavior.NONE;
						}
					}
					else if(callerTM==TMType.BEAN){
						TXContext beanContext = null;
						if((beanContext = BMBeansToTXContexts.get(calleeMethod.getAbstractTypeDeclaration())) != null){
							System.out.println(" -- установили конекст метода в сохраненый БИН контекст компонента.");
							contextSwitch.setContext(beanContext);
							behavior = SwitchBehavior.PROPAGATE;
						} else {
							//ПРосто вызываем БИН как точку входа.
							behavior = SwitchBehavior.NONE;
						}
					}
					} else {
						behavior = SwitchBehavior.NONE;
					}
				}
			   cycleDetected = !putCalleeNodeInCallHistory(calleeNode, DOTGraphExtractor.index, behavior);
				if(cycleDetected){
					Cycle c = new Cycle();
					c.setFirstCallIndex(getFirstCallIndex(calleeNode, behavior));
					contextSwitch.setDecision(c);
				}
				return contextSwitch;
			}
		
		System.out.println("+++ Вызывается НЕКОМПОНЕНТНЫЙ метод "
				+ calleeNode.getName() + " из " 
				+ (callerNode != null ? callerNode.getName() : "null") + " " 
				+ metadata.getJavaOperationToTA() != null ? metadata.getJavaOperationToTA().get(callerMethod) : "noTA");
/*
		contextSwitch = new ContextSwitch();
		contextSwitch.setContext(callerContext);
		contextSwitch.setDecision(null); */
		
		if(callerContext.getTxId() > 0){
			 behavior = SwitchBehavior.PROPAGATE;
		} else {
			 behavior = SwitchBehavior.NONE;
		}
		
		if(callerContext.isUnspecified()){
			if(isMethodDBMeaningful(getNameOnly(calleeNode.getName()))){
				contextSwitch.setDecision(new UnspecifiedBehavior(calleeNode, "Вызов метода в неопределенном\\nтранзакционном контексте"));
			}
		}
		cycleDetected = !putCalleeNodeInCallHistory(calleeNode, DOTGraphExtractor.index, behavior);
		if(cycleDetected){
			Cycle c = new Cycle();
			c.setFirstCallIndex(getFirstCallIndex(calleeNode, behavior));
			contextSwitch.setDecision(c);
		}
		return contextSwitch;
	}

	public ContextSwitch performContainerContextSwitch(TXContext currentContext, CallNode calleeNode) {
		// TODO проверь, что это некомонентный метод и TA для него нету.
		SwitchBehavior behavior = null;
		boolean cycleDetected = false;
		ContextSwitch contextSwitch = null;
		
		TAType ta = javaOperationToTA.get(calleeNode.getJavaMethod());
		if (ta != null) {
			switch (ta) {
				case REQUIRED:
					if (currentContext.getTxId() == TXContext.NO_TRANSACTION_ID) {
						behavior = SwitchBehavior.START_NEW;
						contextSwitch = new ContextSwitch(TXContext.getNewContainerTXContext(), null);
					} else {
						behavior = SwitchBehavior.PROPAGATE;
						contextSwitch = new ContextSwitch(currentContext, null);
					}
					break;
				case REQUIRES_NEW:
					if(currentContext.getTxId() > 0){
						behavior = SwitchBehavior.SUSPEND_AND_START_NEW;
					} else {
						behavior = SwitchBehavior.START_NEW;
					}
					
					contextSwitch = new ContextSwitch(TXContext.getNewContainerTXContext(), null);
					break;
				case MANDATORY:
					if (currentContext.getTxId() == TXContext.NO_TRANSACTION_ID) {
						String message = "Вызов MANDATORY-метода из\\nпустого транзакционного контекста";
						System.out.println(message);
						behavior = SwitchBehavior.NONE;
						contextSwitch = new ContextSwitch(TXContext.NO_TRANSACTION_CONTEXT, new Error(calleeNode, message));
					} else {
						behavior = SwitchBehavior.PROPAGATE;
						contextSwitch = new ContextSwitch(currentContext, null);
					}
					break;
				case SUPPORTS:
					TXContext tc = currentContext;
					if(tc.getTxId() != TXContext.NO_TRANSACTION_ID){
						tc.setUnspecified(true);
						behavior = SwitchBehavior.PROPAGATE;
					} else {
						tc = TXContext.UNSPECIFIED_TRANSACTION_CONTEXT;
						behavior = SwitchBehavior.NONE;
					}
					contextSwitch = new ContextSwitch(tc, null);
					break;
				case NOT_SUPPORTED:
					if(currentContext.getTxId() != TXContext.NO_TRANSACTION_ID){
						behavior = SwitchBehavior.SUSPEND;
					} else {
						behavior = SwitchBehavior.NONE;
					}
					contextSwitch = new ContextSwitch(TXContext.UNSPECIFIED_TRANSACTION_CONTEXT, null);
					break;
				case NEVER:
					if (currentContext.getTxId() == TXContext.NO_TRANSACTION_ID) {
						behavior = SwitchBehavior.NONE;
						//Наверно, стоит засунуть сюда UNSPECIFIED_TRANSACTION_CONTEXT, а не currentContext
						contextSwitch = new ContextSwitch(currentContext, null);
				} else {
					behavior = SwitchBehavior.NONE;
					/*
					 * Выдать ошибку
					 */
					String message = "Вызов NEVER-метода из\\nнепустого транзакционного контекста";
					System.out.println(message);
					
					/* Зачем это тут? И почему index без ++ ?*/
					currentContext.getCallNodesInContext().add(calleeNode);
					currentContext.getNodeIndexes().add(DOTGraphExtractor.index);
					
					contextSwitch = new ContextSwitch(TXContext.NO_TRANSACTION_CONTEXT, new Error(calleeNode, message));
				}
				break;
			}
		} else {
			if(checkIfCallerHasUnspecifiedBehavior(calleeNode)){ // Случай всяких ЖЦ и т.п.
				System.out.println("У нас метод с неопр контекстом. Узнали и вышли.");
				contextSwitch = new ContextSwitch(); 
				contextSwitch.setContext(TXContext.UNSPECIFIED_TRANSACTION_CONTEXT);
				behavior = SwitchBehavior.NONE;
				//return contextSwitch;
			}
		}
		cycleDetected = !putCalleeNodeInCallHistory(calleeNode, DOTGraphExtractor.index, behavior);
		if(cycleDetected){
			Cycle c = new Cycle();
			c.setFirstCallIndex(getFirstCallIndex(calleeNode, behavior));
			contextSwitch.setDecision(c);
		}
		return contextSwitch;
	}

	public ContextSwitch performBeanContextSwitch(CallNode controlMethodNode, TXContext callerContext) {
		String controlMethodName = getNameOnly(controlMethodNode.getName());
		//AbstractMethodDeclaration method = controlMethodNode.getJavaMethod();
		SwitchBehavior behavior = null;
		
		ContextSwitch contextSwitch = null;
		boolean cycleDetected = false;
		
		if (controlMethodName.equals("javax.transaction.UserTransaction :: begin")) {
			if (callerContext.getTxId() == TXContext.NO_TRANSACTION_ID) { //т.е NO_TR_CONT или UNSPECIFIED_CONT
				callerContext = TXContext.getNewBeanTXContext();
				System.out.println("БИН свич начинает новую транзакцию "  + callerContext.getTxId());
				
				behavior = SwitchBehavior.START_NEW;
				
				contextSwitch = new ContextSwitch(callerContext, null);
			} else {
				/*
				 * Если компонент Stateless, то ошибка другая:
				 * javax.ejb.EJBException: Stateless SessionBean method returned without completing transaction
				 */
				String message = "Попытка создать вложенную транзакцию"; //"javax.transaction.NotSupportedException: Nested transaction not supported";
				System.out.println(message);
				
				callerContext = TXContext.NO_TRANSACTION_CONTEXT;
				
				behavior = SwitchBehavior.NONE;
				contextSwitch = new ContextSwitch(callerContext, new Error(controlMethodNode, message));
			}
		}
		if (controlMethodName.equals("javax.transaction.UserTransaction :: commit")|| 
			controlMethodName.equals("javax.transaction.UserTransaction :: rollback")) {
			
			//без этой штуки коммит и ролбэк не заносились в рамки трназакции,
			// в которой были вызваны
			System.out.println("БИН свич завершил имеющуюся транзакцию "  + callerContext.getTxId());
			
			callerContext.getCallNodesInContext().add(controlMethodNode);
			callerContext.getNodeIndexes().add(DOTGraphExtractor.index++); //было index + 1
			callerContext.setUnclosed(false);
			
			if (callerContext.getTxId() != TXContext.NO_TRANSACTION_ID) {
				behavior = SwitchBehavior.PROPAGATE;
				contextSwitch =  new ContextSwitch(TXContext.NO_TRANSACTION_CONTEXT, null);
			} else {
				/*
				 * Если компонент Stateless, то ошибка другая
				 * javax.ejb.EJBException: Stateless SessionBean method returned without completing transaction
				 */
				String message = "Нет транзакции, связанной\\nс потоком выполнения";
				System.out.println(message);
				//"java.lang.IllegalStateException: Transaction is not active in the current thread";
				behavior = SwitchBehavior.NONE;
				contextSwitch = new ContextSwitch(TXContext.NO_TRANSACTION_CONTEXT, new Error(controlMethodNode, message));
			}
		}
		cycleDetected = !putCalleeNodeInCallHistory(controlMethodNode, DOTGraphExtractor.index, behavior);
		if(cycleDetected){
			Cycle c = new Cycle();
			c.setFirstCallIndex(getFirstCallIndex(controlMethodNode, behavior));
			contextSwitch.setDecision(c);
		}
		return contextSwitch;
	}
	
	public Decision callIsProhibited(CallNode caller, CallNode callee){
		Decision decision = null;
		
		if(caller != null){
		Map<TMType, TAType> tmta = typesToTM.get(caller.getJavaMethod().getAbstractTypeDeclaration());
		
		if(tmta != null){
		TMType tm = tmta.keySet().iterator().next();
				if(isMethodProhibitedInCurrentTMType(tm, getNameOnly(callee.getName()))){
						decision = new Error(callee, composeProhibitedErrorMessage(tm));
				}
			}		
		}
		return decision;
	}
	
	private boolean isMethodDBMeaningful(String nameOnly){
		for(String operation: meaningfulDBOperations){
			if(nameOnly.contains(operation)){
				System.out.println("Метод meaningful " + nameOnly);
				return true;
			}
		}
		return false;
	}
	
	public Redundancy inspectTCForRedunancy(TXContext tx){
		for(CallNode node: tx.getCallNodesInContext()){
			if(isMethodDBMeaningful(getNameOnly(node.getName()))){
				return null;
			}
		}
		Redundancy r = new Redundancy();
		r.setDescription("Контекст Т" + tx.getTxId() + " избыточен.");
		return r;
	}
	
	private String composeProhibitedErrorMessage(TMType tm){
		String type = tm == TMType.CONTAINER?"при \\nдекларативном":"при \\nпрограммном";
		return "Вызов метода запрещен " + type + " управлении транзакциями";
	}

	public HashMap<AbstractTypeDeclaration, TXContext> getBMBeansToTXContexts() {
		return BMBeansToTXContexts;
	}

	public static J2EECallGraphMetadata getMetadata() {
		return metadata;
	}

	public List<Map<CallNode, Map<Integer, SwitchBehavior>>> getCallHistory() {
		return callHistory;
	}

	public void setCallHistory(List<Map<CallNode, Map<Integer, SwitchBehavior>>> callHistory) {
		this.callHistory = callHistory;
	}
	
	
	// private List< Map<CallNode, List<SwitchBehavior>> > callHistory = 
	// new ArrayList<Map<CallNode, List<SwitchBehavior>> >();
	public Map<CallNode, Map<Integer, SwitchBehavior>> getCalleeNodeCallsIfItIsAlreadyCalled(CallNode calleeNode){
		for(Map<CallNode, Map<Integer, SwitchBehavior>> calls: callHistory){
			if(calls.keySet().iterator().next().equals(calleeNode)){
				//Map<Integer, SwitchBehavior> entry = calls.get(calleeNode);
				
				return calls;//entry.values().iterator().next();
			}
		}
		return null;
	}
	
	public void removeCalleeBehaviorFromCallHistory(CallNode calleeNode, SwitchBehavior b){
		for(Map<CallNode, Map<Integer, SwitchBehavior>> calls: callHistory){
			if(calls.keySet().iterator().next().equals(calleeNode)){
				
				Map<Integer, SwitchBehavior> callEntry = calls.get(calleeNode);
				
				Iterator<Entry<Integer, SwitchBehavior>> entrySetIt = callEntry.entrySet().iterator();
				
				while(entrySetIt.hasNext()){
					if(entrySetIt.next().getValue().equals(b)){
						System.out.println("Возвращаясь со дна, удалили " + calleeNode.getName() + "+" + b);
						entrySetIt.remove();
						return;
					}
				}
			}
		}
	}
	
	public void removeLastCalleeBehaviorFromCallHistory(CallNode calleeNode){
		System.out.println("Удаляем вхождения для метода " + calleeNode.getName());
		Iterator<Map<CallNode, Map<Integer, SwitchBehavior>>> it = callHistory.iterator();
		while(it.hasNext()){
			Map<CallNode, Map<Integer, SwitchBehavior>> calls = it.next();
			
			
			if(calls.keySet().iterator().next().equals(calleeNode)){
				LinkedHashMap<Integer, SwitchBehavior> occurencies = (LinkedHashMap<Integer, SwitchBehavior>) calls.get(calleeNode);
				System.out.println("LinkedHashMap<Integer, SwitchBehavior> occurencies " + occurencies);
				Iterator<Entry<Integer, SwitchBehavior>> occIt = occurencies.entrySet().iterator();
				
				while(occIt.hasNext()){
					SwitchBehavior beh = occIt.next().getValue();
					if(!occIt.hasNext()){
						System.out.println("Возвращаясь со дна, удалили поведение " + calleeNode.getName() + " " + beh);
						occIt.remove();
					}
				}
				
				if(occurencies.isEmpty()){
					it.remove();
				}
				return;
			}
		}
	}
	
	private boolean putCalleeNodeInCallHistory(CallNode calleeNode, int index, SwitchBehavior behavior){
		
		boolean haveNotCalledYet = true;
		
		System.out.println("=====>>> Записываем вызов метода в историю №" + index + " " + calleeNode.getName() + " - " + behavior);
		
		Map<CallNode, Map<Integer, SwitchBehavior>> call = getCalleeNodeCallsIfItIsAlreadyCalled(calleeNode);
		
		if(call == null){
			call = new LinkedHashMap<CallNode, Map<Integer, SwitchBehavior>>();
			LinkedHashMap<Integer, SwitchBehavior> behaviors = new LinkedHashMap<Integer, SwitchBehavior>();
			behaviors.put(index, behavior);
			call.put(calleeNode, behaviors);
			callHistory.add(call);
		} else if(call.get(calleeNode).values().contains(behavior)){
			System.out.println("Обнаружили цикл!!!! ");
			return haveNotCalledYet = false;
		}
		else {
			call.get(calleeNode).put(index, behavior);
		}
		return haveNotCalledYet;
	}
	
	private int getFirstCallIndex(CallNode calleeNode, SwitchBehavior behavior){
		System.out.println("Пришли в гетФерстКолл с " + behavior);
		for(Map<CallNode,Map<Integer,SwitchBehavior>> m: callHistory){
			for(CallNode c: m.keySet()){
				if(c.equals(calleeNode)){
					Map<Integer,SwitchBehavior> occurencies = m.get(c);
					System.out.println("Наш мэп " + occurencies);
					for(Integer i: occurencies.keySet()){
						System.out.println(i + " " + occurencies.get(i));
						
						if(occurencies.get(i).equals(behavior)){
							return i;
						}
						
					}
				}
			}
			System.out.println();
		}
		return -1;
	}
	
	public boolean checkForUnclosedTransaction(CallNode cn, TXContext currentContext){
		AbstractMethodDeclaration method = cn.getJavaMethod();
		AbstractTypeDeclaration type = method.getAbstractTypeDeclaration();
		ComponentKind kind = Analyzer.getMetadata().getTypeToComponentKind().get(type);
		
		if(kind != ComponentKind.EJB_STATEFUL){

			if (currentContext.getTxId() > 0 && currentContext.getTmType() == TMType.BEAN){
				System.out.println("ВОУ ПОЛЕГЧЕ В СТЕЙТЛЕСЕ НАШЛИ НЕЗАКРЫТУЮ ТРАНЗАКЦИЮ! " + kind + " " + cn.getName());
				getBMBeansToTXContexts().remove(type);
				return true;
			}
		}
		return false;
	}
}
