package ru.vlsu.ispi.txcontexts.analyzer;

import ru.vlsu.ispi.txcontexts.analyzer.decisions.Decision;

public class ContextSwitch {
	public ContextSwitch(){}
	
	public ContextSwitch(TXContext tx, Decision d){
		decision = d;
		context = tx;
	}
	
	
	private Decision decision;
	private TXContext context;
	
	public Decision getDecision() {
		return decision;
	}
	public void setDecision(Decision decision) {
		this.decision = decision;
	}
	public TXContext getContext() {
		return context;
	}
	public void setContext(TXContext context) {
		this.context = context;
	}

}
