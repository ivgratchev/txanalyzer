package ru.vlsu.ispi.txcontexts;

public final class TXContextsConstants {
	public static final String MODEL_EXTENSION = "_txcontexts.xmi"; //$NON-NLS-1$
	public static final String DOT_EXTENSION = "_txcontexts.dot"; //$NON-NLS-1$
}
