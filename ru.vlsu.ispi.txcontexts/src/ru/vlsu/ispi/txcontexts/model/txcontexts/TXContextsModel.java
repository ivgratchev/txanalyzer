/**
 */
package ru.vlsu.ispi.txcontexts.model.txcontexts;

import org.eclipse.emf.common.util.EList;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.CallsModel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TX Contexts Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ru.vlsu.ispi.txcontexts.model.txcontexts.TXContextsModel#getEntryPoints <em>Entry Points</em>}</li>
 * </ul>
 * </p>
 *
 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsPackage#getTXContextsModel()
 * @model
 * @generated
 */
public interface TXContextsModel extends CallsModel {
	/**
	 * Returns the value of the '<em><b>Entry Points</b></em>' containment reference list.
	 * The list contents are of type {@link ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Points</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Points</em>' containment reference list.
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsPackage#getTXContextsModel_EntryPoints()
	 * @model containment="true"
	 * @generated
	 */
	EList<EntryPoint> getEntryPoints();

} // TXContextsModel
