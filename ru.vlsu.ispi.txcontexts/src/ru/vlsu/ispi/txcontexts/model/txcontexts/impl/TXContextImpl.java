/**
 */
package ru.vlsu.ispi.txcontexts.model.txcontexts.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.MethodCall;

import ru.vlsu.ispi.txcontexts.model.txcontexts.TXContext;
import ru.vlsu.ispi.txcontexts.model.txcontexts.TXKind;
import ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TX Context</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TXContextImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TXContextImpl#getCalls <em>Calls</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TXContextImpl extends EObjectImpl implements TXContext {
	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final TXKind KIND_EDEFAULT = TXKind.JTA;

	/**
	 * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected TXKind kind = KIND_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCalls() <em>Calls</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalls()
	 * @generated
	 * @ordered
	 */
	protected EList<MethodCall> calls;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TXContextImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TxcontextsPackage.Literals.TX_CONTEXT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TXKind getKind() {
		return kind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKind(TXKind newKind) {
		TXKind oldKind = kind;
		kind = newKind == null ? KIND_EDEFAULT : newKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TxcontextsPackage.TX_CONTEXT__KIND, oldKind, kind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MethodCall> getCalls() {
		if (calls == null) {
			calls = new EObjectResolvingEList<MethodCall>(MethodCall.class, this, TxcontextsPackage.TX_CONTEXT__CALLS);
		}
		return calls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TxcontextsPackage.TX_CONTEXT__KIND:
				return getKind();
			case TxcontextsPackage.TX_CONTEXT__CALLS:
				return getCalls();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TxcontextsPackage.TX_CONTEXT__KIND:
				setKind((TXKind)newValue);
				return;
			case TxcontextsPackage.TX_CONTEXT__CALLS:
				getCalls().clear();
				getCalls().addAll((Collection<? extends MethodCall>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TxcontextsPackage.TX_CONTEXT__KIND:
				setKind(KIND_EDEFAULT);
				return;
			case TxcontextsPackage.TX_CONTEXT__CALLS:
				getCalls().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TxcontextsPackage.TX_CONTEXT__KIND:
				return kind != KIND_EDEFAULT;
			case TxcontextsPackage.TX_CONTEXT__CALLS:
				return calls != null && !calls.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (kind: ");
		result.append(kind);
		result.append(')');
		return result.toString();
	}

} //TXContextImpl
