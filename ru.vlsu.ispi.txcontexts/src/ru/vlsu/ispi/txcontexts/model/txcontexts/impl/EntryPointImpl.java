/**
 */
package ru.vlsu.ispi.txcontexts.model.txcontexts.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.CallNode;

import ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint;
import ru.vlsu.ispi.txcontexts.model.txcontexts.TXContext;
import ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Entry Point</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link ru.vlsu.ispi.txcontexts.model.txcontexts.impl.EntryPointImpl#getContexts <em>Contexts</em>}</li>
 *   <li>{@link ru.vlsu.ispi.txcontexts.model.txcontexts.impl.EntryPointImpl#getContextChain <em>Context Chain</em>}</li>
 *   <li>{@link ru.vlsu.ispi.txcontexts.model.txcontexts.impl.EntryPointImpl#getRootNode <em>Root Node</em>}</li>
 *   <li>{@link ru.vlsu.ispi.txcontexts.model.txcontexts.impl.EntryPointImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EntryPointImpl extends EObjectImpl implements EntryPoint {
	/**
	 * The cached value of the '{@link #getContexts() <em>Contexts</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContexts()
	 * @generated
	 * @ordered
	 */
	protected EList<TXContext> contexts;

	/**
	 * The cached value of the '{@link #getContextChain() <em>Context Chain</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContextChain()
	 * @generated
	 * @ordered
	 */
	protected EList<TXContext> contextChain;

	/**
	 * The cached value of the '{@link #getRootNode() <em>Root Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootNode()
	 * @generated
	 * @ordered
	 */
	protected CallNode rootNode;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EntryPointImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TxcontextsPackage.Literals.ENTRY_POINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TXContext> getContexts() {
		if (contexts == null) {
			contexts = new EObjectContainmentEList<TXContext>(TXContext.class, this, TxcontextsPackage.ENTRY_POINT__CONTEXTS);
		}
		return contexts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TXContext> getContextChain() {
		if (contextChain == null) {
			contextChain = new EObjectResolvingEList<TXContext>(TXContext.class, this, TxcontextsPackage.ENTRY_POINT__CONTEXT_CHAIN);
		}
		return contextChain;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallNode getRootNode() {
		if (rootNode != null && rootNode.eIsProxy()) {
			InternalEObject oldRootNode = (InternalEObject)rootNode;
			rootNode = (CallNode)eResolveProxy(oldRootNode);
			if (rootNode != oldRootNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TxcontextsPackage.ENTRY_POINT__ROOT_NODE, oldRootNode, rootNode));
			}
		}
		return rootNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallNode basicGetRootNode() {
		return rootNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRootNode(CallNode newRootNode) {
		CallNode oldRootNode = rootNode;
		rootNode = newRootNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TxcontextsPackage.ENTRY_POINT__ROOT_NODE, oldRootNode, rootNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TxcontextsPackage.ENTRY_POINT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TxcontextsPackage.ENTRY_POINT__CONTEXTS:
				return ((InternalEList<?>)getContexts()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TxcontextsPackage.ENTRY_POINT__CONTEXTS:
				return getContexts();
			case TxcontextsPackage.ENTRY_POINT__CONTEXT_CHAIN:
				return getContextChain();
			case TxcontextsPackage.ENTRY_POINT__ROOT_NODE:
				if (resolve) return getRootNode();
				return basicGetRootNode();
			case TxcontextsPackage.ENTRY_POINT__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TxcontextsPackage.ENTRY_POINT__CONTEXTS:
				getContexts().clear();
				getContexts().addAll((Collection<? extends TXContext>)newValue);
				return;
			case TxcontextsPackage.ENTRY_POINT__CONTEXT_CHAIN:
				getContextChain().clear();
				getContextChain().addAll((Collection<? extends TXContext>)newValue);
				return;
			case TxcontextsPackage.ENTRY_POINT__ROOT_NODE:
				setRootNode((CallNode)newValue);
				return;
			case TxcontextsPackage.ENTRY_POINT__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TxcontextsPackage.ENTRY_POINT__CONTEXTS:
				getContexts().clear();
				return;
			case TxcontextsPackage.ENTRY_POINT__CONTEXT_CHAIN:
				getContextChain().clear();
				return;
			case TxcontextsPackage.ENTRY_POINT__ROOT_NODE:
				setRootNode((CallNode)null);
				return;
			case TxcontextsPackage.ENTRY_POINT__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TxcontextsPackage.ENTRY_POINT__CONTEXTS:
				return contexts != null && !contexts.isEmpty();
			case TxcontextsPackage.ENTRY_POINT__CONTEXT_CHAIN:
				return contextChain != null && !contextChain.isEmpty();
			case TxcontextsPackage.ENTRY_POINT__ROOT_NODE:
				return rootNode != null;
			case TxcontextsPackage.ENTRY_POINT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //EntryPointImpl
