/**
 */
package ru.vlsu.ispi.txcontexts.model.txcontexts;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsPackage
 * @generated
 */
public interface TxcontextsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TxcontextsFactory eINSTANCE = ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TxcontextsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>TX Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TX Context</em>'.
	 * @generated
	 */
	TXContext createTXContext();

	/**
	 * Returns a new object of class '<em>Entry Point</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Entry Point</em>'.
	 * @generated
	 */
	EntryPoint createEntryPoint();

	/**
	 * Returns a new object of class '<em>TX Contexts Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TX Contexts Model</em>'.
	 * @generated
	 */
	TXContextsModel createTXContextsModel();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TxcontextsPackage getTxcontextsPackage();

} //TxcontextsFactory
