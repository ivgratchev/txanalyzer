/**
 */
package ru.vlsu.ispi.txcontexts.model.txcontexts;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.MethodcallsPackage;


/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsFactory
 * @model kind="package"
 * @generated
 */
public interface TxcontextsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "txcontexts";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://ispi.vlsu.ru/TXContexts/0.1.incubation/txcontexts";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "txcontexts";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TxcontextsPackage eINSTANCE = ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TxcontextsPackageImpl.init();

	/**
	 * The meta object id for the '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TXContextImpl <em>TX Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TXContextImpl
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TxcontextsPackageImpl#getTXContext()
	 * @generated
	 */
	int TX_CONTEXT = 0;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TX_CONTEXT__KIND = 0;

	/**
	 * The feature id for the '<em><b>Calls</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TX_CONTEXT__CALLS = 1;

	/**
	 * The number of structural features of the '<em>TX Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TX_CONTEXT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.impl.EntryPointImpl <em>Entry Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.impl.EntryPointImpl
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TxcontextsPackageImpl#getEntryPoint()
	 * @generated
	 */
	int ENTRY_POINT = 1;

	/**
	 * The feature id for the '<em><b>Contexts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTRY_POINT__CONTEXTS = 0;

	/**
	 * The feature id for the '<em><b>Context Chain</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTRY_POINT__CONTEXT_CHAIN = 1;

	/**
	 * The feature id for the '<em><b>Root Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTRY_POINT__ROOT_NODE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTRY_POINT__NAME = 3;

	/**
	 * The number of structural features of the '<em>Entry Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTRY_POINT_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TXContextsModelImpl <em>TX Contexts Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TXContextsModelImpl
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TxcontextsPackageImpl#getTXContextsModel()
	 * @generated
	 */
	int TX_CONTEXTS_MODEL = 2;

	/**
	 * The feature id for the '<em><b>Call Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TX_CONTEXTS_MODEL__CALL_NODES = MethodcallsPackage.CALLS_MODEL__CALL_NODES;

	/**
	 * The feature id for the '<em><b>Root Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TX_CONTEXTS_MODEL__ROOT_NODES = MethodcallsPackage.CALLS_MODEL__ROOT_NODES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TX_CONTEXTS_MODEL__NAME = MethodcallsPackage.CALLS_MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Entry Points</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TX_CONTEXTS_MODEL__ENTRY_POINTS = MethodcallsPackage.CALLS_MODEL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>TX Contexts Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TX_CONTEXTS_MODEL_FEATURE_COUNT = MethodcallsPackage.CALLS_MODEL_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.TXKind <em>TX Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TXKind
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TxcontextsPackageImpl#getTXKind()
	 * @generated
	 */
	int TX_KIND = 3;


	/**
	 * Returns the meta object for class '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.TXContext <em>TX Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TX Context</em>'.
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TXContext
	 * @generated
	 */
	EClass getTXContext();

	/**
	 * Returns the meta object for the attribute '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.TXContext#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TXContext#getKind()
	 * @see #getTXContext()
	 * @generated
	 */
	EAttribute getTXContext_Kind();

	/**
	 * Returns the meta object for the reference list '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.TXContext#getCalls <em>Calls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Calls</em>'.
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TXContext#getCalls()
	 * @see #getTXContext()
	 * @generated
	 */
	EReference getTXContext_Calls();

	/**
	 * Returns the meta object for class '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint <em>Entry Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entry Point</em>'.
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint
	 * @generated
	 */
	EClass getEntryPoint();

	/**
	 * Returns the meta object for the containment reference list '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint#getContexts <em>Contexts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contexts</em>'.
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint#getContexts()
	 * @see #getEntryPoint()
	 * @generated
	 */
	EReference getEntryPoint_Contexts();

	/**
	 * Returns the meta object for the reference list '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint#getContextChain <em>Context Chain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Context Chain</em>'.
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint#getContextChain()
	 * @see #getEntryPoint()
	 * @generated
	 */
	EReference getEntryPoint_ContextChain();

	/**
	 * Returns the meta object for the reference '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint#getRootNode <em>Root Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Root Node</em>'.
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint#getRootNode()
	 * @see #getEntryPoint()
	 * @generated
	 */
	EReference getEntryPoint_RootNode();

	/**
	 * Returns the meta object for the attribute '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint#getName()
	 * @see #getEntryPoint()
	 * @generated
	 */
	EAttribute getEntryPoint_Name();

	/**
	 * Returns the meta object for class '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.TXContextsModel <em>TX Contexts Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TX Contexts Model</em>'.
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TXContextsModel
	 * @generated
	 */
	EClass getTXContextsModel();

	/**
	 * Returns the meta object for the containment reference list '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.TXContextsModel#getEntryPoints <em>Entry Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Entry Points</em>'.
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TXContextsModel#getEntryPoints()
	 * @see #getTXContextsModel()
	 * @generated
	 */
	EReference getTXContextsModel_EntryPoints();

	/**
	 * Returns the meta object for enum '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.TXKind <em>TX Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>TX Kind</em>'.
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TXKind
	 * @generated
	 */
	EEnum getTXKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TxcontextsFactory getTxcontextsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TXContextImpl <em>TX Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TXContextImpl
		 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TxcontextsPackageImpl#getTXContext()
		 * @generated
		 */
		EClass TX_CONTEXT = eINSTANCE.getTXContext();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TX_CONTEXT__KIND = eINSTANCE.getTXContext_Kind();

		/**
		 * The meta object literal for the '<em><b>Calls</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TX_CONTEXT__CALLS = eINSTANCE.getTXContext_Calls();

		/**
		 * The meta object literal for the '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.impl.EntryPointImpl <em>Entry Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.impl.EntryPointImpl
		 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TxcontextsPackageImpl#getEntryPoint()
		 * @generated
		 */
		EClass ENTRY_POINT = eINSTANCE.getEntryPoint();

		/**
		 * The meta object literal for the '<em><b>Contexts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTRY_POINT__CONTEXTS = eINSTANCE.getEntryPoint_Contexts();

		/**
		 * The meta object literal for the '<em><b>Context Chain</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTRY_POINT__CONTEXT_CHAIN = eINSTANCE.getEntryPoint_ContextChain();

		/**
		 * The meta object literal for the '<em><b>Root Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTRY_POINT__ROOT_NODE = eINSTANCE.getEntryPoint_RootNode();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTRY_POINT__NAME = eINSTANCE.getEntryPoint_Name();

		/**
		 * The meta object literal for the '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TXContextsModelImpl <em>TX Contexts Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TXContextsModelImpl
		 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TxcontextsPackageImpl#getTXContextsModel()
		 * @generated
		 */
		EClass TX_CONTEXTS_MODEL = eINSTANCE.getTXContextsModel();

		/**
		 * The meta object literal for the '<em><b>Entry Points</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TX_CONTEXTS_MODEL__ENTRY_POINTS = eINSTANCE.getTXContextsModel_EntryPoints();

		/**
		 * The meta object literal for the '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.TXKind <em>TX Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TXKind
		 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.impl.TxcontextsPackageImpl#getTXKind()
		 * @generated
		 */
		EEnum TX_KIND = eINSTANCE.getTXKind();

	}

} //TxcontextsPackage
