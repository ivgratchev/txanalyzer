/**
 */
package ru.vlsu.ispi.txcontexts.model.txcontexts;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.CallNode;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entry Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint#getContexts <em>Contexts</em>}</li>
 *   <li>{@link ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint#getContextChain <em>Context Chain</em>}</li>
 *   <li>{@link ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint#getRootNode <em>Root Node</em>}</li>
 *   <li>{@link ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsPackage#getEntryPoint()
 * @model
 * @generated
 */
public interface EntryPoint extends EObject {
	/**
	 * Returns the value of the '<em><b>Contexts</b></em>' containment reference list.
	 * The list contents are of type {@link ru.vlsu.ispi.txcontexts.model.txcontexts.TXContext}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contexts</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contexts</em>' containment reference list.
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsPackage#getEntryPoint_Contexts()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<TXContext> getContexts();

	/**
	 * Returns the value of the '<em><b>Context Chain</b></em>' reference list.
	 * The list contents are of type {@link ru.vlsu.ispi.txcontexts.model.txcontexts.TXContext}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context Chain</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context Chain</em>' reference list.
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsPackage#getEntryPoint_ContextChain()
	 * @model
	 * @generated
	 */
	EList<TXContext> getContextChain();

	/**
	 * Returns the value of the '<em><b>Root Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Node</em>' reference.
	 * @see #setRootNode(CallNode)
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsPackage#getEntryPoint_RootNode()
	 * @model
	 * @generated
	 */
	CallNode getRootNode();

	/**
	 * Sets the value of the '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint#getRootNode <em>Root Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Node</em>' reference.
	 * @see #getRootNode()
	 * @generated
	 */
	void setRootNode(CallNode value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsPackage#getEntryPoint_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // EntryPoint
