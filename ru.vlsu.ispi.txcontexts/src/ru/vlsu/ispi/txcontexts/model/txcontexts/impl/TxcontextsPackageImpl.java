/**
 */
package ru.vlsu.ispi.txcontexts.model.txcontexts.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.MethodcallsPackage;

import ru.vlsu.ispi.txcontexts.model.txcontexts.EntryPoint;
import ru.vlsu.ispi.txcontexts.model.txcontexts.TXContext;
import ru.vlsu.ispi.txcontexts.model.txcontexts.TXContextsModel;
import ru.vlsu.ispi.txcontexts.model.txcontexts.TXKind;
import ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsFactory;
import ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TxcontextsPackageImpl extends EPackageImpl implements TxcontextsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass txContextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass entryPointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass txContextsModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum txKindEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TxcontextsPackageImpl() {
		super(eNS_URI, TxcontextsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TxcontextsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TxcontextsPackage init() {
		if (isInited) return (TxcontextsPackage)EPackage.Registry.INSTANCE.getEPackage(TxcontextsPackage.eNS_URI);

		// Obtain or create and register package
		TxcontextsPackageImpl theTxcontextsPackage = (TxcontextsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TxcontextsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TxcontextsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		MethodcallsPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theTxcontextsPackage.createPackageContents();

		// Initialize created meta-data
		theTxcontextsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTxcontextsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TxcontextsPackage.eNS_URI, theTxcontextsPackage);
		return theTxcontextsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTXContext() {
		return txContextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTXContext_Kind() {
		return (EAttribute)txContextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTXContext_Calls() {
		return (EReference)txContextEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEntryPoint() {
		return entryPointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEntryPoint_Contexts() {
		return (EReference)entryPointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEntryPoint_ContextChain() {
		return (EReference)entryPointEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEntryPoint_RootNode() {
		return (EReference)entryPointEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntryPoint_Name() {
		return (EAttribute)entryPointEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTXContextsModel() {
		return txContextsModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTXContextsModel_EntryPoints() {
		return (EReference)txContextsModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTXKind() {
		return txKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TxcontextsFactory getTxcontextsFactory() {
		return (TxcontextsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		txContextEClass = createEClass(TX_CONTEXT);
		createEAttribute(txContextEClass, TX_CONTEXT__KIND);
		createEReference(txContextEClass, TX_CONTEXT__CALLS);

		entryPointEClass = createEClass(ENTRY_POINT);
		createEReference(entryPointEClass, ENTRY_POINT__CONTEXTS);
		createEReference(entryPointEClass, ENTRY_POINT__CONTEXT_CHAIN);
		createEReference(entryPointEClass, ENTRY_POINT__ROOT_NODE);
		createEAttribute(entryPointEClass, ENTRY_POINT__NAME);

		txContextsModelEClass = createEClass(TX_CONTEXTS_MODEL);
		createEReference(txContextsModelEClass, TX_CONTEXTS_MODEL__ENTRY_POINTS);

		// Create enums
		txKindEEnum = createEEnum(TX_KIND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MethodcallsPackage theMethodcallsPackage = (MethodcallsPackage)EPackage.Registry.INSTANCE.getEPackage(MethodcallsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		txContextsModelEClass.getESuperTypes().add(theMethodcallsPackage.getCallsModel());

		// Initialize classes and features; add operations and parameters
		initEClass(txContextEClass, TXContext.class, "TXContext", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTXContext_Kind(), this.getTXKind(), "kind", null, 0, 1, TXContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTXContext_Calls(), theMethodcallsPackage.getMethodCall(), null, "calls", null, 0, -1, TXContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(entryPointEClass, EntryPoint.class, "EntryPoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEntryPoint_Contexts(), this.getTXContext(), null, "contexts", null, 0, -1, EntryPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getEntryPoint_ContextChain(), this.getTXContext(), null, "contextChain", null, 0, -1, EntryPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEntryPoint_RootNode(), theMethodcallsPackage.getCallNode(), null, "rootNode", null, 0, 1, EntryPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEntryPoint_Name(), ecorePackage.getEString(), "name", null, 0, 1, EntryPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(txContextsModelEClass, TXContextsModel.class, "TXContextsModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTXContextsModel_EntryPoints(), this.getEntryPoint(), null, "entryPoints", null, 0, -1, TXContextsModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(txKindEEnum, TXKind.class, "TXKind");
		addEEnumLiteral(txKindEEnum, TXKind.NONE);
		addEEnumLiteral(txKindEEnum, TXKind.JTA);
		addEEnumLiteral(txKindEEnum, TXKind.LOCAL);

		// Create resource
		createResource(eNS_URI);
	}

} //TxcontextsPackageImpl
