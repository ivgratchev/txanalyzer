/**
 */
package ru.vlsu.ispi.txcontexts.model.txcontexts;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.modisco.usecase.modelfilter.methodcalls.methodcalls.MethodCall;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TX Context</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ru.vlsu.ispi.txcontexts.model.txcontexts.TXContext#getKind <em>Kind</em>}</li>
 *   <li>{@link ru.vlsu.ispi.txcontexts.model.txcontexts.TXContext#getCalls <em>Calls</em>}</li>
 * </ul>
 * </p>
 *
 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsPackage#getTXContext()
 * @model
 * @generated
 */
public interface TXContext extends EObject {
	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link ru.vlsu.ispi.txcontexts.model.txcontexts.TXKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TXKind
	 * @see #setKind(TXKind)
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsPackage#getTXContext_Kind()
	 * @model
	 * @generated
	 */
	TXKind getKind();

	/**
	 * Sets the value of the '{@link ru.vlsu.ispi.txcontexts.model.txcontexts.TXContext#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TXKind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(TXKind value);

	/**
	 * Returns the value of the '<em><b>Calls</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.gmt.modisco.usecase.methodcalls.model.methodcalls.MethodCall}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calls</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calls</em>' reference list.
	 * @see ru.vlsu.ispi.txcontexts.model.txcontexts.TxcontextsPackage#getTXContext_Calls()
	 * @model
	 * @generated
	 */
	EList<MethodCall> getCalls();

} // TXContext
